﻿using API.Models.Entidades.Display;
using API.Models.Entidades.Global;
using API.Models.Repositorios;
using Global.Entidades;
using System.Collections.Generic;
namespace API.Models.Negocios
{
    public class NegStore
    {
        private RepStore repStore;
        private NegUser negUser;
        public NegStore() 
        {
            this.repStore = new RepStore();
            this.negUser = new NegUser();
        }
        public List<Language> Language(int locationId)
        {
                return this.repStore.Language(locationId);
        }
        public List<Group> Store(int idLocation, int idLanguage, int IdUser)
        {
            if (IdUser > 0)
            {
                this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, IdUser));
            }
            return this.repStore.Store(idLocation, idLanguage, IdUser);
        }
        public void Favorite(int idItem, int idUser)
        {
            this.repStore.Favorite(idItem, idUser);
        }
        public void Viewed(int idLocation, int idUser)
        {
            this.repStore.Viewed(idLocation, idUser);
        }
    }
}
