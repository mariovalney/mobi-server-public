﻿using API.Models.Entidades;
using API.Models.Entidades.Rating;
using API.Models.Repositorios;
using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLine.Models.Sistema;

namespace API.Models.Negocios
{
    public class NegSurvey
    {
        private RepSurvey repSurvey;
        private NegUser negUser;
        
        public NegSurvey() 
        {
            this.repSurvey = new RepSurvey();
            this.negUser = new NegUser();
        }

        public Rating Rating(int idLocation, int idUser)
        {
            this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, idUser));
            var rating = this.repSurvey.Rating(idLocation);
            if (rating == null)
            {
                rating = new Rating();
                rating.SurveyQuestions = new List<SurveyQuestion>();
            }
            return rating;
        }
        public int Appreciation(Appreciation appreciation, DateTime date)
        {
            return this.repSurvey.Appreciation(appreciation, date);
        }
        public void Answer(List<SurveyAnswer> answers, DateTime date)
        {
            foreach (var i in answers)
            {
                this.repSurvey.Answer(i, date);
            }
        }
    }
}
