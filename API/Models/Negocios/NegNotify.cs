﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Entidades.Notify;
using API.Models.Repositorios;
using Boundary.Models.Entidades;
using Error;
using Global.Entidades;
using System;
using System.Collections.Generic;

namespace API.Models.Negocios
{
    public class NegNotify
    {
        private RepNotify repNotify;
        private NegUser negUser;
        public NegNotify() 
        {
            this.repNotify = new RepNotify();
            this.negUser = new NegUser();
        }
        public List<Why> Why()
        {
            return this.repNotify.Why();
        }
        public void Read(int userId, int notifyId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            this.repNotify.Read(userId, notifyId, app);
        }
        public void Remove(int userId, int notifyId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            this.repNotify.Remove(userId, notifyId, app);
        }
        public void Block(int userId, int locationId, int reasonId, string type, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            if (string.IsNullOrWhiteSpace(type))
            {
                throw new InvalidOperationException("Não foi possível determinar o tipo correto de operação");
            }
            this.repNotify.Block(userId, locationId, reasonId, type.Trim(), app);
        }
        public int Count(APP app, int userId, NotifyFilter filter)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            return this.repNotify.Count(app, userId, filter);
        }
        public List<Message> See(int userId, int page, APP app, int pageSize)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, userId));
            return this.repNotify.See(userId, page, app, pageSize);
        }
    }
}