﻿using API.Models.Entidades;
using API.Models.Repositorios;
using Boundary.Models.Entidades;
using Boundary.Models.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using Global.Utils;
using Global.Entidades;
using Error;
using API.Models.Entidades.Account;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Flurl.Http;
using Newtonsoft.Json;

namespace API.Models.Negocios
{
    public class NegScore
    {
        private RepScore repScore;
        private string charList = string.Empty;
        private string secretList = string.Empty;
        private int nBase;
        private NegUser negUser;
        private NegFranchise negFranchise;
        public NegScore()
        {
            this.repScore = new RepScore();
            this.charList = "999999999999999999999999999999999999999999999999999999999999999999999999999999";
            this.secretList = "999999999999999999999999999999999999999999999999999999999999999999999999999999";
            this.nBase = this.charList.Length;
            this.negUser = new NegUser();
            this.negFranchise = new NegFranchise();
        }
        public List<ExtractApp> ExtractApp(User user, APP app, DateTime selectedDate)
        {
            return this.repScore.ExtractApp(user, app, selectedDate);
        }
        public DateTime ExtractMinDate(User user, APP app)
        {
            return this.repScore.ExtractMinDate(user, app);
        }
        public Lote Lote(string qrCode)
        {
            var loteId = default(int);
            var lote = default(Lote);
            if (string.IsNullOrEmpty(qrCode))
            {
                throw new QrCodeInvalidException("Formato incorreto do código de resgate.");
            }
            if (qrCode.Split('_').Length != 2)
            {
                throw new QrCodeInvalidException("Formato incorreto do código de resgate.");
            }
            if (!qrCode.Split('_')[0].Equals("ExampleClubQrCodeTroca"))
            {
                throw new QrCodeInvalidException("Formato incorreto do código de resgate.");
            }
            if (!int.TryParse(qrCode.Split('_')[1], out loteId))
            {
                throw new QrCodeInvalidException("Formato incorreto do código de resgate.");
            }

            lote = this.repScore.Lote(new Lote(loteId));

            if (lote == null)
            {
                throw new LoteNotFoundException("O lote não foi encontrado.");
            }
            if (lote.Enabled == 0)
            {
                throw new LoteNotEnableException("O lote encontra-se desativado.");
            }
            else if (lote.Enabled == 2)
            {
                throw new LoteNotEnableException("O lote encontra-se recolhido.");
            }
            else if (lote.Enabled == 4)
            {
                throw new LoteNotEnableException("O lote encontra-se com o código de resgate desativado.");
            }
            return lote;
        }
        public List<string> Description(Outlet outlet)
        {
            if (outlet == null)
            {
                throw new LocationInvalidException("Outlet Desconhecido.");
            }
            return this.repScore.Description(outlet);
        }
        public int Score(int userId, int locationId, APP app)
        {
            if(app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            return this.repScore.Score(userId, locationId, app);
        }
        public List<Extract> Extract(int locationId, int userId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, userId));
            this.negFranchise.Find(new Outlet(locationId));
            return this.repScore.Extract(userId, locationId, app);
        }
        public GainScore Gain(GainScore gainScore, APP app)
        {
            if(app == null)
            {
                throw new AppInvalidException();
            }
            if (string.IsNullOrWhiteSpace(gainScore.Code))
            {
                throw new QrCodeInvalidException();
            }
            else
            {
                gainScore.Code = gainScore.Code.Replace('_', '+');
                gainScore.Code = gainScore.Code.Replace(' ', '+');
            }
            if (gainScore.Code.ToLower().Contains("nfce"))
            {
                gainScore.FROM = GainScore.GainScoreType.NFCe;
                gainScore = NFCe(gainScore);
                gainScore.PlusScore = this.repScore.Gain(gainScore, app);
                return gainScore;
            }
            if (gainScore.Code.Length == 24)
            {
                gainScore.FROM = GainScore.GainScoreType.BAR;
                gainScore = Bar(gainScore);
                gainScore.PlusScore = this.repScore.Gain(gainScore, app);
                return gainScore;
            }
            else if (gainScore.Code.Length < 32)
            {
                throw new QrCodeInvalidException();
            }
            else if (gainScore.Code.Length == 32)
            {
                gainScore.FROM = GainScore.GainScoreType.PALETA;
                gainScore.PlusScore = this.repScore.Gain(gainScore, app);
                return gainScore;
            }
            else if (gainScore.Code.Length == 40 && gainScore.Code.Substring(0, 8).ToLower().Equals("exampleclub"))
            {
                gainScore.FROM = GainScore.GainScoreType.PALETA;
                gainScore.Code = gainScore.Code.Substring(8, 32);
                gainScore.PlusScore = this.repScore.Gain(gainScore, app);
                return gainScore;
            }
            else
            {
                gainScore.FROM = GainScore.GainScoreType.COUPON;
                gainScore = Coupon(gainScore);
                gainScore.PlusScore = this.repScore.Gain(gainScore, app);
                return gainScore;
            }
        }
        private string PutMaskCNPJ(string cnpj_without_mask)
        {
            if (string.IsNullOrWhiteSpace(cnpj_without_mask))
            {
                throw new QrCodeInvalidException("Código com CNPJ inválido.");
            }
            else if(!cnpj_without_mask.All(char.IsDigit))
            {
                throw new QrCodeInvalidException("Código com CNPJ inválido.");
            }
            else if (cnpj_without_mask.Length != 14)
            {
                throw new QrCodeInvalidException("Código com CNPJ inválido.");
            }
            string cnpj = "";
            cnpj = cnpj_without_mask.Substring(0, 2) + ".";
            cnpj = cnpj + cnpj_without_mask.Substring(2, 3) + ".";
            cnpj = cnpj + cnpj_without_mask.Substring(5, 3) + "/";
            cnpj = cnpj + cnpj_without_mask.Substring(8, 4) + "-";
            cnpj = cnpj + cnpj_without_mask.Substring(12, 2);
            return cnpj;
        }
        private string PutMaskCPF(string cpf_without_mask)
        {
            if (string.IsNullOrWhiteSpace(cpf_without_mask))
            {
                throw new QrCodeInvalidException("Código com CPF inválido.");
            }
            else if (!cpf_without_mask.All(char.IsDigit))
            {
                throw new QrCodeInvalidException("Código com CPF inválido.");
            }
            else if (cpf_without_mask.Length != 11)
            {
                throw new QrCodeInvalidException("Código com CPF inválido.");
            }
            string cpf = "";
            cpf = cpf_without_mask.Substring(0, 3) + ".";
            cpf = cpf + cpf_without_mask.Substring(3, 3) + ".";
            cpf = cpf + cpf_without_mask.Substring(6, 3) + "-";
            cpf = cpf + cpf_without_mask.Substring(9, 2);
            return cpf;
        }
        private decimal MakeMoney(string money_as_str)
        {
            if (string.IsNullOrWhiteSpace(money_as_str))
            {
                throw new QrCodeInvalidException("O valor da conta não é válido.");
            }
            else if (!money_as_str.All(char.IsDigit))
            {
                throw new QrCodeInvalidException("O valor da conta não é válido.");
            }
            if (money_as_str.Length > 7)
            {
                throw new QrCodeInvalidException("O valor da conta não é válido.");
            }
            for (int i = money_as_str.Length; i < 7; i++)
            {
                money_as_str = "0" + money_as_str;
            }
            var high = decimal.Parse(money_as_str.Substring(0, 5));
            var low = decimal.Parse(money_as_str.Substring(5, 2)) / 100;
            return (high + low);
        }
        public DateTime MakeDateTime(string date, string time)
        {
            try
            {
                int size = date.Length;
                for (int i = 0; i < 6 - size; i++)
                {
                    date = "0" + date;
                }
                size = time.Length;
                for (int i = 0; i < 6 - size; i++)
                {
                    time = time + "0";
                }
                //código de barra utiliza o formato de data dd/mm/yy
                if (date.Length == 6)
                {
                    DateTime dt = new DateTime
                    (
                        (int.Parse(date.Substring(4, 2)) + 2000),
                        int.Parse(date.Substring(2, 2)),
                        int.Parse(date.Substring(0, 2)),
                        int.Parse(time.Substring(0, 2)),
                        int.Parse(time.Substring(2, 2)),
                        int.Parse(time.Substring(4, 2))
                    );
                    return dt;
                }
                //qrcode utiliza o formato de data dd/mm/yyyy
                else
                {
                    DateTime dt = new DateTime
                    (
                        int.Parse(date.Substring(4, 4)),
                        int.Parse(date.Substring(2, 2)),
                        int.Parse(date.Substring(0, 2)),
                        int.Parse(time.Substring(0, 2)),
                        int.Parse(time.Substring(2, 2)),
                        int.Parse(time.Substring(4, 2))
                    );
                    return dt;
                }
            }
            catch (Exception)
            {
                throw new QrCodeInvalidException("Data e/ou Hora inválida.");
            }
        }
        private int ToDecimal(string input)
        {
            var input_reverse = input.Reverse();
            int result = 0;
            int pos = 0;
            foreach (char c in input_reverse)
            {
                result += this.charList.IndexOf(c) * (int)Math.Pow(this.nBase, pos);
                pos++;
            }
            return result;
        }
        private GainScore NFCe(GainScore gainScore)
        {
            #region get info
            var nfcKey = @"([0-9]{2})([0-9]{4})([0-9]{14})([0-9]{2})([0-9]{3})([0-9]{9})([0-9]{1})([0-9]{8})([0-9]{1})";

            // Busca os dados da chave de acesso.
            var match = Regex.Match(gainScore.Code, nfcKey);

            if(!match.Success)
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }

            var dataNFC = GetNFCeInformations(match.Value);

            var value = (decimal) dataNFC.total_da_nota;
            var datetime = match.Groups[2].Value;
            var cnpj = PutMaskCNPJ(match.Groups[3].Value);
            var cpf = string.IsNullOrWhiteSpace(dataNFC.cpf_cnpj_destinatario) ? "00000000000" : dataNFC.cpf_cnpj_destinatario;
            var api = match.Groups[4].Value;
            int efc_as_int;
            var efc = match.Groups[5].Value;
            var cod = match.Groups[6].Value;
            var at = DateTime.ParseExact(string.Format("20{0}01", datetime), "yyyyMMdd", null);

            #endregion
            gainScore.Coupon = new FiscalCoupon();
            gainScore.Coupon.At = at;
            gainScore.Coupon.SyncAt = Auxiliar.GetCurrentDateTime();
            gainScore.Coupon.ECF = efc;
            gainScore.Coupon.Value = value;
            gainScore.Coupon.QrCode = gainScore.Code;
            gainScore.Coupon.Employee = 0;
            try
            {
                efc_as_int = int.Parse(efc);
                gainScore.Coupon.API = int.Parse(api);
                gainScore.Coupon.Cod = int.Parse(cod);
            }
            catch (Exception)
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }
            if (gainScore.Coupon.Cod <= 0)
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }
            if (gainScore.Coupon.Value <= 0)
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }
            if (efc_as_int <= 0)
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }
            //validando campos
            if (cpf != null && cpf.Equals("00000000000"))
            {
                gainScore.Coupon.Document = new Document(null, cnpj);
                if (!gainScore.Coupon.Document.IsValidDocument())
                {
                    throw new DocumentInvalidException("CNPJ inválido " + gainScore.Coupon.Document.CNPJ);
                }
            }
            else
            {
                gainScore.Coupon.Document = new Document(cpf, cnpj);
                if (!gainScore.Coupon.Document.CNPJIsValid(gainScore.Coupon.Document.CNPJ))
                {
                    throw new DocumentInvalidException("CNPJ inválido");
                }
                if (!gainScore.Coupon.Document.CPFIsValid(gainScore.Coupon.Document.CPF))
                {
                    throw new DocumentInvalidException("CPF inválido");
                }
            }
            return gainScore;
        }

        private dynamic GetNFCeInformations(string nfcCode)
        {
            var resultToken = "https://www.portalmenew.com.br/public-api/"
                .WithTimeout(10)
                .WithHeader("Content-Type", "application/json")
                .PostJsonAsync(new
                {
                    token = string.Empty,
                    requests = new
                    {
                        jsonrpc = "2.0",
                        method = "Usuario/login",
                        @params = new
                        {
                            usuario = "7rUw04rXKr",
                            token = "5walfe2v0e$16p134v0n1i&42n$cri09"
                        },
                        id = "3226798"
                    }
                })
                .ReceiveJson().Result.result;

            var dataNFC = "https://www.portalmenew.com.br/public-api/"
                .WithTimeout(10)
                .WithHeader("Content-Type", "application/json")
                .PostJsonAsync(new
                {
                    token = resultToken,
                    requests = new
                    {
                        jsonrpc = "2.0",
                        method = "Chave_nfce/get",
                        @params = new
                        {
                            chave = nfcCode
                        },
                        id = "3226798"
                    }
                })
                .ReceiveJson().Result.result;

            if(Enumerable.Count(dataNFC) > 0)
            {
                return dataNFC[0];
            }
            else
            {
                throw new QrCodeInvalidException("O código do cupom fiscal não é valido");
            }
        }

        private GainScore Bar(GainScore gainScore)
        {
            string x_rotation = gainScore.Code.Substring(0, 1);
            gainScore.Code = Global.Security.Security.GetInstance().rotation(gainScore.Code, secretList.IndexOf(x_rotation));
            string s4_from_code = gainScore.Code.Substring(1, 1);
            string s5_from_code = gainScore.Code.Substring(2, 1);

            int api = ToDecimal(gainScore.Code.Substring(3, 1)); //api
            if (api == 1)
            {
                int src = ToDecimal(gainScore.Code.Substring(4, 1)); //src
                int lId = ToDecimal(gainScore.Code.Substring(5, 2)); //lid

                int cod = ToDecimal(gainScore.Code.Substring(7, 5)); //cod
                int efc = ToDecimal(gainScore.Code.Substring(12, 3)); //ecf
                int val = ToDecimal(gainScore.Code.Substring(15, 4)); //val

                int dat = ToDecimal(gainScore.Code.Substring(19, 3)); //data
                int tim = ToDecimal(gainScore.Code.Substring(22, 2)); //tempo
                int att = 0;

                int x = (int)(api + lId + src + cod + efc + val % 99 + dat % 167 + tim % 999) % this.nBase;
                int s1 = (int)(api * 2 + src + byte.MaxValue + lId * 3) % this.nBase;
                int s2 = (int)(cod + efc + val + byte.MaxValue) % this.nBase;
                int s3 = (int)(dat + tim * 3) % this.nBase;
                int s4 = (((s1 * s1) % byte.MaxValue) + (s2 * 2) + (s3 * 3) + ((x * x * x) % byte.MaxValue)) % this.nBase;
                int s5 = (((s1 * s4) / 2) + (s2 * s3) + ((x * s3 * s4) / 3)) % this.nBase;

                if (gainScore.Code[0] != this.secretList[x])
                {
                    throw new Exception("Código S0 inválido");
                }
                if (gainScore.Code[1] != this.secretList[s4])
                {
                    throw new Exception("Código S4 inválido");
                }
                if (gainScore.Code[2] != this.secretList[s5])
                {
                    throw new Exception("Código S4 inválido");
                }
                gainScore.LocationId = lId;
                gainScore.Coupon = new FiscalCoupon();
                gainScore.Coupon.At = this.MakeDateTime(dat.ToString(), tim.ToString());
                gainScore.Coupon.SyncAt = Auxiliar.GetCurrentDateTime();
                gainScore.Coupon.ECF = efc.ToString();

                int size = gainScore.Coupon.ECF.Length;
                for (int i = 0; i < 3 - size; i++)
                {
                    gainScore.Coupon.ECF = "0" + gainScore.Coupon.ECF;
                }
                gainScore.Coupon.API = api;
                gainScore.Coupon.Cod = cod;

                gainScore.Coupon.Value = MakeMoney(val.ToString());
                gainScore.Coupon.QrCode = gainScore.Code;
                gainScore.Coupon.Employee = att;

                if (gainScore.Coupon.Cod <= 0)
                {
                    throw new QrCodeInvalidException("O código do cupom fiscal não é valido: " + cod);
                }
                if (gainScore.Coupon.Value <= 0)
                {
                    throw new QrCodeInvalidException("O cupom fiscal possui valor menor ou igual a zero " + gainScore.Coupon.Value + ".");
                }
                gainScore.Coupon.Company = FachadaBoundary.GetInstance().Find<FiscalCompany>(new FiscalCompany(src), Search.Parameter.Id);
                if (gainScore.Coupon.Company == null)
                {
                    throw new FiscalCompanyNotFoundException("Empresa fiscal não encontrada: " + src);
                }
                else if (!gainScore.Coupon.Company.Enabled)
                {
                    throw new FiscalCompanyBlockedException("Empresa fiscal desativada: " + gainScore.Coupon.Company.Name);
                }
                return gainScore;
            }
            else
            {
                throw new QrCodeInvalidException("API do cupom fiscal não é válido " + api + ".");
            }
        }
        private GainScore Coupon(GainScore gainScore)
        {
            var api  = string.Empty;
            var hash = string.Empty;
            var cnpj = string.Empty;
            var cpf  = string.Empty;
            var cod  = string.Empty;
            var data = string.Empty;
            var time = string.Empty;
            var val  = string.Empty;
            var att  = string.Empty;
            var efc  = string.Empty;
            try
            {
                gainScore.Code = Global.Security.Security.GetInstance().Decrypt(gainScore.Code);
            }
            catch (FormatException)
            {
                throw new QrCodeInvalidException("Código encontra-se fora da base 64.");
            }
            catch (CryptographicException)
            {
                throw new QrCodeInvalidException("Erro de criptografia");
            }
            api = gainScore.Code.Substring(0, 2);
            if (api.Equals("01"))
            {
                if (gainScore.Code.Length != 86)
                {
                    throw new QrCodeInvalidException("Tamanho inválido.");
                }
                //get the pieces of code
                hash = gainScore.Code.Substring(2, 8);
                cnpj = gainScore.Code.Substring(10, 14);
                cpf  = gainScore.Code.Substring(24, 11);
                cod  = gainScore.Code.Substring(35, 6);
                data = gainScore.Code.Substring(41, 8);
                time = gainScore.Code.Substring(49, 6);
                val  = gainScore.Code.Substring(55, 7);
                att  = gainScore.Code.Substring(62, 4);
                efc  = gainScore.Code.Substring(66, 20);

                //fill the object

                gainScore.Coupon = new FiscalCoupon();
                gainScore.Coupon.QrCode = gainScore.Code;
                gainScore.Coupon.At = this.MakeDateTime(data, time);
                gainScore.Coupon.Value = MakeMoney(val);
                gainScore.Coupon.SyncAt  = Auxiliar.GetCurrentDateTime();
                gainScore.Coupon.ECF     = efc;
                gainScore.Coupon.API     = int.Parse(api);
                try
                {
                    gainScore.Coupon.Cod = int.Parse(cod);
                }
                catch (Exception)
                {
                    throw new QrCodeInvalidException("Número de série inválido.");
                }
                try
                {
                    gainScore.Coupon.Employee = int.Parse(att);
                }
                catch (Exception)
                {
                    throw new QrCodeInvalidException("Atendente inválido.");
                }
            }
            else if (api.Equals("02"))
            {
                if (gainScore.Code.Length != 72)
                {
                    throw new QrCodeInvalidException("Tamanho inválido.");
                }
                //get the pieces of code
                hash = gainScore.Code.Substring(2, 8);
                cnpj = gainScore.Code.Substring(10, 14);
                cpf  = gainScore.Code.Substring(24, 11);
                cod  = gainScore.Code.Substring(35, 9);
                data = gainScore.Code.Substring(44, 8);
                time = gainScore.Code.Substring(52, 6);
                val  = gainScore.Code.Substring(58, 7);
                att  = gainScore.Code.Substring(65, 4);
                efc  = gainScore.Code.Substring(69, 3);

                //fill the object
                gainScore.Coupon       = new FiscalCoupon();
                gainScore.Coupon.At    = this.MakeDateTime(data, time);
                gainScore.Coupon.Value = this.MakeMoney(val);
                gainScore.Coupon.SyncAt = Auxiliar.GetCurrentDateTime();
                gainScore.Coupon.ECF = efc;
                gainScore.Coupon.QrCode = gainScore.Code;
                gainScore.Coupon.API = int.Parse(api);
                try
                {
                    if (int.Parse(efc) <= 0)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    throw new QrCodeInvalidException("Número de série inválido.");
                }
                try
                {
                    gainScore.Coupon.Cod = int.Parse(cod);
                }
                catch (Exception)
                {
                    throw new QrCodeInvalidException("Número da nota inválido.");
                }
                try
                {
                    gainScore.Coupon.Employee = int.Parse(att);
                }
                catch (Exception)
                {
                    throw new QrCodeInvalidException("Atendente inválido.");
                }
            }
            else
            {
                throw new QrCodeInvalidException("API inválida.");
            }
            if (gainScore.Coupon.Cod <= 0)
            {
                throw new QrCodeInvalidException("Número da nota inválido.");
            }
            if (gainScore.Coupon.Value <= 0)
            {
                throw new QrCodeInvalidException("Valor inválido.");
            }
            //validando campos
            if (cpf != null && (cpf.Equals("00000000000") || cpf.Equals("00000000191")))
            {
                gainScore.Coupon.Document = new Document(null, PutMaskCNPJ(cnpj));
                if (!gainScore.Coupon.Document.IsValidDocument())
                {
                    throw new DocumentInvalidException("CNPJ inválido.");
                }
            }
            else
            {
                gainScore.Coupon.Document = new Document(PutMaskCPF(cpf), PutMaskCNPJ(cnpj));
                if (!gainScore.Coupon.Document.CNPJIsValid(gainScore.Coupon.Document.CNPJ))
                {
                    throw new DocumentInvalidException("CNPJ inválido.");
                }
                if (!gainScore.Coupon.Document.CPFIsValid(gainScore.Coupon.Document.CPF))
                {
                    throw new DocumentInvalidException("CPF inválido.");
                }
            }
            var company = new FiscalCompany();
            company.Hash = hash;
            gainScore.Coupon.Company = FachadaBoundary.GetInstance().Find<FiscalCompany>(company, Search.Parameter.HASH);
            if (gainScore.Coupon.Company == null)
            {
                throw new FiscalCompanyNotFoundException("Empresa fiscal não encontrada.");
            }
            else if (!gainScore.Coupon.Company.Enabled)
            {
                throw new FiscalCompanyBlockedException("Empresa fiscal desativada.");
            }
            return gainScore;
        }
    }
}