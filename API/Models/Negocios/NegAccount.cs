﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Repositorios;
using Error;
using Global.Entidades;
using Global.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API.Models.Negocios
{
    public class NegAccount
    {
        private RepAccount repAccount;
        public NegAccount() 
        {
            this.repAccount = new RepAccount();
        }
        public Account RequestPassword(string email)
        {
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new EmailInvalidException("Email vazio.");
            }
            email = email.Trim().ToLower();
            if (!rg.IsMatch(email))
            {
                throw new EmailFormatInvalidException("O email informado não é válido.");
            }
            var foundAccount   = default(Account);
            var currentAccount = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.EMAIL, email));
            if (currentAccount == null)
            {
                throw new AccountNotFoundException("O usuário não foi encontrado.");
            }
            else
            {
                if (currentAccount.BlockedAsAccount)
                {
                    throw new AccountBlockedException("O usuário encontra-se bloqueado.");
                }
                if (currentAccount.ExpirationToken >= Auxiliar.GetCurrentDateTime().AddMinutes(-60))
                {
                    return currentAccount;
                }
                else
                {
                    do
                    {
                        currentAccount.PasswordToken = Auxiliar.GerarToken(32);
                        foundAccount = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.PASSWORD_TOKEN, currentAccount.PasswordToken));
                    }
                    while (foundAccount != null);
                    return this.repAccount.UpdateToken(currentAccount);
                }
            }
        }
    }
}
