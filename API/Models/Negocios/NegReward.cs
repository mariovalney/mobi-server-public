﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Entidades.Premium;
using API.Models.Repositorios;
using Boundary.Models.Entidades;
using Error;
using Global.Entidades;
using Global.Utils;
using System;
using System.Collections.Generic;
using TimeLine.Models.Entidades;
using TimeLine.Models.Sistema;
namespace API.Models.Negocios
{
    public class NegReward
    {
        private RepReward     repReward;
        private RepRewardBuy  repRewardBuy;
        private RepRewardShot repRewardShot;
        private RepRewardGift repRewardGift;
        private NegUser negUser;
        private NegFranchise negFranchise;
        public NegReward()
        {
            this.repRewardBuy  = new RepRewardBuy();
            this.repRewardGift = new RepRewardGift();
            this.repRewardShot = new RepRewardShot();
            this.repReward     = new RepReward();
            this.negUser       = new NegUser();
            this.negFranchise = new NegFranchise();
        }
        public List<AwardId> Award(int userId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, userId));
            return this.repReward.Award(userId, app);
        }
        public AwardBuy Buy(int rewardBuyId, int userId, int locationId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.negFranchise.Find(new Outlet(locationId));
            return this.repRewardBuy.Buy(rewardBuyId, userId, locationId, app);
        }
        public AwardId Shot(string qrCode, int userId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            else if (string.IsNullOrEmpty(qrCode))
            {
                throw new QrCodeInvalidException("O código informado é vazio ou nulo.");
            }
            else
            {
                qrCode = qrCode.Trim();
            }
            if(qrCode.Length == 32 || qrCode.Length == 40)
            {
                if (qrCode.Length == 32 && qrCode.Substring(0, 4).ToLower().Equals("shot"))
                {
                    return this.repRewardShot.Shot(qrCode, userId, app);
                }
                else if (qrCode.Length == 40 && qrCode.Substring(0, 12).ToLower().Equals("exampleclubshot"))
                {
                    return this.repRewardShot.Shot(qrCode.Substring(8, 32), userId, app);
                }
                else
                {
                    throw new QrCodeInvalidException("Código com prefixo inválido");
                }
            }
            else
            {
                throw new QrCodeInvalidException("Código com " + qrCode.Length + " caracteres");
            }
        }
        public void Discarded(int idToRescue, bool discarded, string type, int userId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.repReward.Discarded(idToRescue, discarded, type, userId, app);
        }
        public List<RewardBuy> ShowCase(int userId, int locationId, APP app)
        {
            if (app == null)
            {
                throw new AppInvalidException("Aplicativo desconhecido");
            }
            this.negFranchise.Find(new Outlet(locationId));
            if (userId > 0)
            {
                this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, userId));
            }
            return this.repRewardBuy.ShowCase(locationId);
        }
        public bool Belongs(Reward reward, Franchise franchise)
        {
            bool belongs = false;
            if (reward == null)
            {
                return false;
            }
            else if (reward.OutletId == franchise.Locations[0].Id || reward.FranchiseId == franchise.Id)
            {
                belongs = true;
            }
            return belongs;
        }
        public AwardInfo Rescue(Franchise franchise, string code, RescueFrom from)
        {
            return Rescue(franchise, code, int.MaxValue, from);
        }
        public AwardInfo Check(Franchise franchise, string code)
        {
            return Check(franchise, code, int.MinValue);
        }
        public AwardInfo Rescue(Franchise franchise, string code, int userId, RescueFrom from)
        {
            var info = Check(franchise, code, userId);
            if (info.Award.Type == "RewardBuy")
            {
                this.repReward.Rescue("RewardBuy", info.Award.IdToRescue, franchise.Locations[0].Id, from);
            }
            else if (info.Award.Type == "RewardGift")
            {
                this.repReward.Rescue("RewardGift", info.Award.IdToRescue, franchise.Locations[0].Id, from);
            }
            else if (info.Award.Type == "RewardShot")
            {
                this.repReward.Rescue("RewardShot", info.Award.IdToRescue, franchise.Locations[0].Id, from);
            }
            else
            {
                throw new RewardTypeException("Recompensa com tipo não reconhecido. ");
            }
            TL tl = new TL
            {
                LocationId = franchise.Locations[0].Id,
                UserId = info.User.Id,
                When = Auxiliar.GetCurrentDateTime(),
                RewardTitle = info.Award.Title,
                Type = (int)TL.ActionType.REWARD_RESCUE,
            };
            FachadaTimeLine.GetInstance().Save(tl);
            this.negUser.Customer(info.User.Id, franchise.Locations[0].Id);
            info.Award.Redeemed = true;
            info.Award.RedeemedAt = Auxiliar.GetCurrentDateTime();
            return info;
        }
        public AwardInfo Check(Franchise franchise, string code, int userId)
        {
            var info = default(AwardInfo);
            var type = string.Empty;
            this.negFranchise.Find(new Outlet(franchise.Locations[0].Id));
            if (userId != int.MaxValue)
            {
                this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, userId));
            }
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new RewardCodeEmptyException("Código inválido. (vazio)");
            }
            switch (code.ToUpper().Substring(code.Length - 1, 1))
            {
                case "B":
                    type = "RewardBuy";
                    info = this.repRewardBuy.AwardInfo(Auxiliar.unMaskReward(code.ToUpper().Substring(0, code.Length - 1)));
                    break;
                case "G":
                    type = "RewardGift";
                    info = this.repRewardGift.AwardInfo(Auxiliar.unMaskReward(code.ToUpper().Substring(0, code.Length - 1)));
                    break;
                case "S":
                    type = "RewardShot";
                    info = this.repRewardShot.AwardInfo(Auxiliar.unMaskReward(code.ToUpper().Substring(0, code.Length - 1)));
                    break;
                default:
                    throw new RewardNotFoundException("Código inválido.");
            }
            if (info == null)
            {
                throw new RewardNotFoundException("Recompensa não encontrada.");
            }
            else if (userId != int.MaxValue && info.User.Id != userId)
            {
                throw new RewardLinkUserException("A recompensa " + code + " não pertence ao usuário.");
            }
            else if (userId == int.MaxValue && (info.User.BlockedAsAccount || info.User.BlockedAsUser))
            {
                throw new AccountBlockedException("O usuário encontra-se bloqueado.");
            }
            else if (!Belongs(info.Award, franchise))
            {
                throw new BelongsException("Recompensa " + code + " Não pertence a loja " + franchise.Name + " - " + franchise.Locations[0].Reference);
            }
            else if (info.Award.RedeemedAt > DateTime.MinValue)
            {
                throw new RewardAlreadyRescuedException("A recompensa: " + code + " encontra-se resgatada.");
            }
            else if (info.Award.ExpirationAt < Auxiliar.GetCurrentDateTime())
            {
                throw new RewardExpiredException("Recompensa " + code + " encontra-se expirada.");
            }
            return info;
        }
    }
}
