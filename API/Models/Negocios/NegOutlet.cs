﻿
using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Repositorios;
using Boundary.Models.Entidades;
namespace API.Models.Negocios
{
    public class NegOutlet
    {
        private RepOutlet repOutlet;
        private RepScore repScore;
        public NegOutlet() 
        {
            this.repOutlet = new RepOutlet();
            this.repScore = new RepScore();
        }
        public Outlet FindById(Outlet outlet, APP app, User user)
        {
            outlet = this.repOutlet.FindById(outlet, app, user);
            if (user.Id > 0)
            {
                ExtractResume scores = this.repScore.Resume(user, outlet);
                if (scores != null)
                {
                    outlet.Score = scores.Score;
                    outlet.HasReward = scores.HasReward;
                }
            }
            return outlet;
        }
    }
}
