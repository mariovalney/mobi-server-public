﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Repositorios;
using Error;
using Global.Entidades;
using Global.Security;
using Global.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API.Models.Negocios
{
    public class NegUser
    {
        private RepUser    repUser;
        private RepAccount repAccount;
        public NegUser() 
        {
            this.repUser    = new RepUser();
            this.repAccount = new RepAccount();
        }
        public User Photo(User user)
        {
            return this.repUser.Photo(user);
        }
        public bool VerifyPassword(User tryLogin, Account foundAccount)
        {
            try
            {
                bool isValid = true;
                if (foundAccount.Password == null)
                {
                    isValid = false;
                }
                else if (!CryptoSHA512Hash.IsSameHash(tryLogin.Email + tryLogin.Password, foundAccount.Password))
                {
                    isValid = false;
                }
                return isValid;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void Customer(int userId, int LocationId)
        {
            this.repUser.Customer(userId, LocationId);
        }
        public User Find(SearchAccount search, bool check_return = true)
        {
            var user = this.repUser.Find(search);
            if (check_return)
            {
                if (user == null)
                {
                    throw new AccountNotFoundException("O usuário não foi encontrado.");
                }
                else if (user.BlockedAsUser || user.BlockedAsAccount)
                {
                    throw new AccountBlockedException("O usuário encontra-se bloqueado.");
                }
            }
            return user;
        }
        public User UpdateCPF(User user)
        {
            if (user == null)
            {
                throw new NullReferenceException("Objeto não foi instanciado corretamente");
            }
            var document = new Document(user.CPF, null); 
            if (!document.CPFIsValid(document.CPF))
            {
                throw new DocumentInvalidException("O CPF informado é inválido.");
            }
            User currentUser = this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
            Account account  = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.CPF, user.CPF));
            if (account != null && account.IdAccount != currentUser.IdAccount)
            {
                throw new DocumentAlreadyExists("O CPF informado já está sendo utilizado.");
            }
            else
            {
                currentUser = this.repUser.UpdateCPF(user);
            }
            return currentUser;
        }
        public User Edit(User user)
        {
            if (user.Birthday != DateTime.MinValue)
            {
                if (user.Birthday >= Auxiliar.GetCurrentDateTime())
                {
                    throw new BirthdayInvalidException("Data de nascimento inválida.");
                }
                else if (user.Birthday <= Auxiliar.GetCurrentDateTime().AddYears(-100))
                {
                    throw new BirthdayInvalidException("Data de nascimento inválida");
                }
            }
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                throw new NameInvalidException("Campo 'nome' não informado.");
            }
            else
            {
                user.Name = user.Name.Trim();
                var name_pieces = user.Name.Split(' ');
                for (var i = 0; i < name_pieces.Length; i++)
                {
                    name_pieces[i] = name_pieces[i].Substring(0, 1).ToUpper() + name_pieces[i].Substring(1, name_pieces[i].Length - 1).ToLower();
                }
                user.Name = string.Join(" ", name_pieces);
            }
            if (string.IsNullOrWhiteSpace(user.Gender))
            {
                user.Gender = "";
            }
            else if (!user.Gender.Trim().Equals("male") && !user.Gender.Trim().Equals("female"))
            {
                throw new GenderInvalidException("O campo 'sexo' não foi preenchido com um valor válido.");
            }
            user.Gender = user.Gender.Trim();
            if (!string.IsNullOrWhiteSpace(user.Document.CPF) && !user.Document.CPFIsValid(user.Document.CPF))
            {
                throw new DocumentInvalidException("O CPF informado não é válido");
            }
            User user_saved = this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
            if (string.IsNullOrWhiteSpace(user.Document.CPF) && user_saved.Document.CPFIsValid(user_saved.Document.CPF))
            {
                throw new DocumentRemoveException("Não é possível remover o CPF do cadastro");
            }
            else if (!string.IsNullOrWhiteSpace(user.Document.CPF) && user.Document.CPFIsValid(user.Document.CPF))
            {
                Account account = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.CPF, user.Document.CPF));
                if (account != null && account.IdAccount != user_saved.IdAccount)
                {
                    throw new DocumentAlreadyExists("O CPF informado já está sendo utilizado.");
                }
            }
            user.Email = user_saved.Email;
            if (string.IsNullOrWhiteSpace(user.Hometown))
            {
                user.Hometown = user_saved.Hometown;
            }
            if (string.IsNullOrWhiteSpace(user.Location))
            {
                user.Location = user_saved.Location;
            }
            if (string.IsNullOrWhiteSpace(user.Locale))
            {
                user.Locale = user_saved.Locale;
            }
            if (string.IsNullOrWhiteSpace(user.FacebookAccessToken))
            {
                user.FacebookAccessToken = user_saved.FacebookAccessToken;
            }
            if (user.FacebookId == 0)
            {
                user.FacebookId = user_saved.FacebookId;
            }
            if (user.FacebookAccessExpires == 0)
            {
                user.FacebookAccessExpires = user.FacebookAccessExpires;
            }
            if (user.Document == null || !user.Document.CPFIsValid(user.Document.CPF))
            {
                user.CPF = user_saved.Document.CPF;
            }
            user.Email = user_saved.Email;
            return this.repUser.Update(user);
        }
        public User Save(User user)
        {
            var userFoundEmail  = default(User);
            var userFoundCPF    = default(User);
            var accountEmail    = default(Account);
            var accountCPF      = default(Account); 
            var rg_email = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            if (user.Birthday != DateTime.MinValue)
            {
                if (user.Birthday >= Auxiliar.GetCurrentDateTime())
                {
                    throw new BirthdayInvalidException("Data de nascimento inválida.");
                }
                if (user.Birthday.Date == Auxiliar.GetCurrentDateTime().Date)
                {
                    throw new BirthdayInvalidException("Data de nascimento inválida.");
                }
                else if (user.Birthday <= Auxiliar.GetCurrentDateTime().AddYears(-100))
                {
                    throw new BirthdayInvalidException("Data de nascimento inválida");
                }
            }
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                throw new NameInvalidException("Campo 'nome' não informado.");
            }
            else
            {
                user.Name = user.Name.Trim();
                var name_pieces = user.Name.Split(' ');
                for (var i = 0; i < name_pieces.Length; i++)
                {
                    name_pieces[i] = name_pieces[i].Substring(0, 1).ToUpper() + name_pieces[i].Substring(1, name_pieces[i].Length - 1).ToLower();
                }
                user.Name = string.Join(" ", name_pieces);
            }
            if (string.IsNullOrWhiteSpace(user.Email))
            {
                throw new EmailInvalidException("Campo 'email' não foi informado.");
            }
            user.Email = user.Email.Trim().ToLower();
            if (!rg_email.IsMatch(user.Email))
            {
                throw new EmailFormatInvalidException("O campo 'email' não foi preenchido com um email válido.");
            }
            if (string.IsNullOrWhiteSpace(user.Gender))
            {
                user.Gender = string.Empty;
            }
            else if (!user.Gender.Trim().ToLower().Equals("male") && !user.Gender.Trim().ToLower().Equals("female"))
            {
                throw new GenderInvalidException("O campo 'sexo' não foi preenchido com um valor válido.");
            }
            user.Gender = user.Gender.Trim().ToLower();
            if (string.IsNullOrWhiteSpace(user.Password))
            {
                throw new PasswordInvalidException("O campo 'password' não foi informado.");
            }
            else 
            {
                user.EncodePassword();
            }
            if (!string.IsNullOrWhiteSpace(user.CPF))
            {
                user.CPF = user.CPF.Trim();
            }
            else 
            {
                user.CPF = string.Empty;
            }
            if (!string.IsNullOrWhiteSpace(user.CPF) && !user.Document.CPFIsValid(user.CPF))
            {
                throw new DocumentInvalidException("O CPF informado não é válido");
            }
            userFoundEmail = this.Find(new SearchAccount(SearchAccount.ParameterType.EMAIL, user.Email), false);
            if (!string.IsNullOrWhiteSpace(user.CPF))
            {
                userFoundCPF = this.Find(new SearchAccount(SearchAccount.ParameterType.CPF, user.CPF),   false);
            }
            if (userFoundEmail != null) 
            {
                throw new AccountAlreadyExistsException("Já existe um perfil de usuário com o Email.");
            }
            if (userFoundCPF != null)
            {
                throw new DocumentAlreadyExists("Já existe um perfil de usuário com o CPF.");
            }
            else
            {
                accountEmail = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.EMAIL, user.Email));
                if (!string.IsNullOrWhiteSpace(user.CPF))
                {
                    accountCPF = this.repAccount.Find(new SearchAccount(SearchAccount.ParameterType.CPF, user.CPF));
                }
                if (accountEmail == null && accountCPF == null)
                {
                    return this.repUser.Save(user);
                }
                if (accountEmail == null && accountCPF != null && VerifyPassword(user, accountCPF))
                {
                    user.IdAccount = accountCPF.IdAccount;
                    return this.repUser.Profile(user);
                }
                if (accountEmail == null && accountCPF != null && !VerifyPassword(user, accountCPF))
                {
                    throw new DocumentAlreadyExists("Já existe um perfil de usuário com o CPF.");
                }
                if (accountEmail != null && accountCPF == null && VerifyPassword(user, accountEmail))
                {
                    user.IdAccount = accountEmail.IdAccount;
                    user = this.repUser.Profile(user);
                    if (!string.IsNullOrWhiteSpace(user.CPF))
                    {
                        return this.UpdateCPF(user);
                    }
                    else 
                    {
                        return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
                    }
                }
                if (accountEmail != null && accountCPF == null && !VerifyPassword(user, accountEmail))
                {
                    throw new AccountAlreadyExistsException("Já existe um perfil de usuário com o Email: " + user.Email + ".");
                }
                if (accountEmail != null && accountCPF != null && accountCPF.IdAccount == accountEmail.IdAccount &&  VerifyPassword(user, accountEmail)) 
                {
                    user.IdAccount = accountCPF.IdAccount;
                    return this.repUser.Profile(user);
                }
                else
                {
                    throw new AccountAlreadyExistsException("Já existe um perfil de usuário com o Email: " + user.Email + ".");
                }
            }
        }
    }
}
