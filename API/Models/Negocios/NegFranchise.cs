﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Entidades.Search;
using API.Models.Repositorios;
using Boundary.Models.Entidades;
using Error;
using Global.Entidades;
using System.Collections.Generic;
namespace API.Models.Negocios
{
    public class NegFranchise
    {
        private RepFranchise repFranchise;
        private RepMap repMap;
        private NegUser negUser;
        private RepScore repScore;

        public NegFranchise()
        {
            this.negUser = new NegUser();
            this.repMap = new RepMap();
            this.repScore = new RepScore();
            this.repFranchise = new RepFranchise();
        }
        public List<Map> Map(int userId, APP app, string search, double lon, double lat)
        {
            if (app == default(APP))
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id,userId));
            var map = this.repMap.Map(app, userId, search, lon, lat);
            List<ExtractResume> scores = this.repScore.Resume(userId);
            if (scores.Count > 0)
            {
                foreach (var fr in map)
                {
                    foreach (var loc in fr.Locations)
                    {
                        if (scores.Find(x => x.OutletId == loc.Id) != null)
                        {
                            loc.Score = scores.Find(x => x.OutletId == loc.Id).Score;
                            loc.HasReward = scores.Find(x => x.OutletId == loc.Id).HasReward;
                        }
                    }
                }
            }
            return map;
        }
        /*public List<Franchise> ToBlock(int userId, string hashApp)
        {
            return repEstablishment.ToBlock(userId, hashApp);
        }*/
        public Franchise Find(Outlet outlet)
        {
            var franchise = this.repFranchise.Find(outlet);
            if (franchise == null)
            {
                throw new LocationNotFoundException("Loja não foi encontrada.");
            }
            if (franchise.Locations == null && franchise.Locations.Count == 0)
            {
                throw new LocationNotFoundException("Loja não foi encontrada.");
            }
            if (!franchise.Enabled || !franchise.Locations[0].Enabled)
            {
                throw new LocationBlockedException("Loja desativada");
            }
            return franchise;
        }
        public Franchise Find(Document document)
        {
            Franchise franchise;
            if (document == null)
            {
                throw new DocumentInvalidException("CNPJ inválido. (documento null)");
            }
            if (string.IsNullOrWhiteSpace(document.CNPJ))
            {
                throw new DocumentInvalidException("CNPJ inválido. (vazio)");
            }
            if (!document.IsValidDocument())
            {
                throw new DocumentInvalidException("CNPJ inválido: " + document.CNPJ + ".");
            }
            franchise = this.repFranchise.Find(document);
            if (franchise == null)
            {
                throw new LocationNotFoundException("Não foi encontrado nenhuma loja com o CNPJ: " + document.CNPJ + ".");
            }
            if (!franchise.Enabled || !franchise.Locations[0].Enabled)
            {
                throw new LocationBlockedException("Loja de CNPJ " + document.CNPJ + " está desativada");
            }
            return franchise;
        }
        public Franchise Find(int userId, string code, OutletSearch search)
        {
            var franchise = this.repFranchise.Find(userId, code, search);
            List<ExtractResume> scores = this.repScore.Resume(userId, franchise.Id);
            if (scores.Count > 0)
            {
                foreach (var loc in franchise.Locations)
                {
                    if (scores.Find(x => x.OutletId == loc.Id) != null)
                    {
                        loc.Score = scores.Find(x => x.OutletId == loc.Id).Score;
                        loc.HasReward = scores.Find(x => x.OutletId == loc.Id).HasReward;
                    }
                }
            }
            return franchise;
        }
        public List<Franchise> Home(APP app, int idUser, string search, double lat, double lon)
        {
            if (app == default(APP))
            {
                throw new AppInvalidException("Aplicativo inválido.");
            }
            List<Franchise> franchises = this.repFranchise.Home(app, idUser, search, lat, lon);
            if (idUser > 0)
            {
                this.negUser.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, idUser));
                List<ExtractResume> scores = this.repScore.Resume(idUser);
                if (scores.Count > 0)
                {
                    foreach (var fr in franchises)
                    {
                        foreach (var loc in fr.Locations)
                        {
                            if (scores.Find(x => x.OutletId == loc.Id) != null)
                            {
                                loc.Score = scores.Find(x => x.OutletId == loc.Id).Score;
                                loc.HasReward = scores.Find(x => x.OutletId == loc.Id).HasReward;
                            }
                        }
                    }
                }
            }
            return franchises;
        }
    }
}
