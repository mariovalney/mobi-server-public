﻿using API.Models.Entidades;
using API.Models.Entidades.Contact;
using API.Models.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Negocios
{
    public class NegContact
    {
        private RepContact repContact;
        public NegContact() 
        {
            this.repContact = new RepContact();
        }
        public List<Phone> Phone(Outlet outlet)
        {
            return this.repContact.Phone(outlet);
        }
        public Address Address(Outlet outlet)
        {
            return this.repContact.Address(outlet);
        }
        public List<OpenHours> Hours(Outlet outlet)
        {
            return this.repContact.Hours(outlet);
        }
    }
}