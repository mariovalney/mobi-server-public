﻿using API.Models.Entidades;
using API.Models.Entidades.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public List<Phone> Phone(Outlet outlet) 
        {
            return this.negContact.Phone(outlet);
        }
        public Address Address(Outlet outlet)
        {
            return this.negContact.Address(outlet);
        }
        public List<OpenHours> Hours(Outlet outlet)
        {
            return this.negContact.Hours(outlet);
        }
    }
}
