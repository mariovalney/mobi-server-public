﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using Boundary.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public Outlet FindById(Outlet outlet, APP app, User user)
        {
            return this.negOutlet.FindById(outlet, app, user);
        }
    }
}
