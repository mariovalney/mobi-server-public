﻿using API.Models.Entidades.Rating;
using System;
using System.Collections.Generic;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public Rating Rating(int idLocation, int idUser)
        {
            return this.negSurvey.Rating(idLocation, idUser);
        }
        public void Answer(List<SurveyAnswer> answers, DateTime date)
        {
            this.negSurvey.Answer(answers, date);
        }
        public int Appreciation(Appreciation appreciation, DateTime date)
        {
            return this.negSurvey.Appreciation(appreciation, date);
        }
    }
}
