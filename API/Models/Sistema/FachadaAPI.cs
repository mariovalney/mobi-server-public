﻿using API.Models.Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        private static FachadaAPI fachada;
        private NegFranchise negFranchise;
        private NegOutlet negOutlet;
        private NegScore negScore;
        private NegNotify negNotify;
        private NegContact negContact;
        //private NegFacebook negFacebook;
        private NegReward negReward;
        private NegStore negStore;
        private NegSurvey negSurvey;
        private NegUser negUser;
        private NegAccount negAccount;

        private FachadaAPI()
        {
            this.negFranchise = new NegFranchise();
            this.negOutlet = new NegOutlet();
            this.negScore = new NegScore();
            this.negContact = new NegContact();
            this.negNotify = new NegNotify();
            this.negReward = new NegReward();
            this.negStore = new NegStore();
            this.negSurvey = new NegSurvey();
            this.negAccount = new NegAccount();
            this.negUser = new NegUser();
        }
        public static FachadaAPI GetInstance()
        {
            if (fachada == null)
            {
                fachada = new FachadaAPI();
            }
            return fachada;
        }
    }
}