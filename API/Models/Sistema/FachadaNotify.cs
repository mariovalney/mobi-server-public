﻿using API.Models.Entidades;
using API.Models.Entidades.Notify;
using Boundary.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public List<Why> Why()
        {
            return this.negNotify.Why();
        }
        public void Read(int userId, int notifyId, APP app)
        {
            this.negNotify.Read(userId, notifyId, app);
        }
        public void Remove(int userId, int notifyId, APP app)
        {
            this.negNotify.Remove(userId, notifyId, app);
        }
        public void Block(int userId, int locationId, int reasonId, string type, APP app)
        {
            this.negNotify.Block(userId, locationId, reasonId, type, app);
        }
        public int Count(APP app, int userId, NotifyFilter filter)
        {
            return this.negNotify.Count(app, userId, filter);
        }
        public List<Message> See(int userId, int page, APP app, int pageSize)
        {
            return this.negNotify.See(userId, page, app, pageSize);
        }
    }
}
