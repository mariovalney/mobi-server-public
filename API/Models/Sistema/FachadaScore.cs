﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using Boundary.Models.Entidades;
using System;
using System.Collections.Generic;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public List<ExtractApp> ExtractApp(User user, APP app, DateTime selectedDate)
        {
            return this.negScore.ExtractApp(user, app, selectedDate);
        }
        public DateTime ExtractMinDate(User user, APP app)
        {
            return this.negScore.ExtractMinDate(user, app);
        }
        public Lote Lote(string qrCode)
        {
            return this.negScore.Lote(qrCode);
        }
        public List<string> Description(Outlet outlet)
        {
            return this.negScore.Description(outlet);
        }
        public int Score(int userId, int locationId, APP app)
        {
            return this.negScore.Score(userId, locationId, app);
        }
        public List<Extract> Extract(int userId, int locationId, APP app)
        {
            return this.negScore.Extract(locationId, userId, app);
        }
        public GainScore Gain(GainScore gainScore, APP app)
        {
            return this.negScore.Gain(gainScore, app);
        }
    }
}
