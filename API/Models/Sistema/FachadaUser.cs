﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public User Find(SearchAccount search, bool check_user = true)
        {
            return this.negUser.Find(search, check_user);
        }
        public User Edit(User user)
        {
            return this.negUser.Edit(user);
        }
        public User UpdateCPF(User user)
        {
            return this.negUser.UpdateCPF(user);
        }
        public User Save(User user)
        {
            return this.negUser.Save(user);
        }
        public Account RequestPassword(string email)
        {
            return this.negAccount.RequestPassword(email);
        }
        public User Photo(User user)
        {
            return this.negUser.Photo(user);
        }

        /*
         */ 

        /*
        public void SaveCustomer(int userId, int LocationId)
        {
            this.negUser.SaveCustomer(userId, LocationId);
        }

        public UserWS FindById(int id)
        {
            return this.negUser.FindById(id);
        }
        public UserWS FindByCPF(string cpf)
        {
            return this.negUser.FindByCPF(cpf);
        }
    */}
}
