﻿using API.Models.Entidades;
using API.Models.Entidades.Search;
using Boundary.Models.Entidades;
using Global.Entidades;
using System.Collections.Generic;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        ///<summary>Função responsável por retornar a localização de todas as lojas vinculadas ao aplicativo informado.</summary>
        ///<param name="userId">Identificador do usuário.</param>
        ///<param name="app">Aplicativo utilizado pelo usuário.</param>
        ///<returns>Lista completa de todas as lojas cadastradas no aplicativo.</returns>
        public List<Map> Map(int userId, APP app, string search = "", double lon = 0, double lat = 0)
        {
            return this.negFranchise.Map(userId, app, search, lon, lat);
        }
        public List<Franchise> Home(APP app, int idUser, string search, double lat, double lon)
        {
            return negFranchise.Home(app, idUser, search, lat, lon);
        }
        public Franchise Find(Document document)
        {
            return this.negFranchise.Find(document);
        }
        public Franchise Find(int userId, string code, OutletSearch search)
        {
            return this.negFranchise.Find(userId, code, search);
        }
    }
}
