﻿using API.Models.Entidades.Display;
using API.Models.Entidades.Global;
using System.Collections.Generic;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public List<Group> Store(int idLocation, int idLanguage, int idUser)
        {
            return this.negStore.Store(idLocation, idLanguage, idUser);
        }
        public List<Language> Language(int locationId)
        {
            return this.negStore.Language(locationId);
        }
        public void Favorite(int idItem, int idUser)
        {
            this.negStore.Favorite(idItem, idUser);
        }
        public void Viewed(int idLocation, int idUser)
        {
            this.negStore.Viewed(idLocation, idUser);
        }
    }
}
