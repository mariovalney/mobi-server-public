﻿using API.Models.Entidades;
using API.Models.Entidades.Premium;
using Boundary.Models.Entidades;
using System.Collections.Generic;

namespace API.Models.Sistema
{
    public partial class FachadaAPI
    {
        public AwardInfo CheckToRescue(Franchise franchise, string code)
        {
            return this.negReward.Check(franchise, code, int.MaxValue);
        }
        public AwardInfo RescueReward(Franchise franchise, string code, RescueFrom from)
        {
            return this.negReward.Rescue(franchise, code, from);
        }
        public AwardInfo RescueReward(Franchise franchise, string code, int userId, RescueFrom from)
        {
            return this.negReward.Rescue(franchise, code, userId, from);
        }
        public List<RewardBuy> ShowCase(int userId, int locationId, APP app)
        {
            return this.negReward.ShowCase(userId, locationId, app);
        }
        public void Discarded(int idToRescue, bool discarded, string type, int userId, APP app)
        {
            this.negReward.Discarded(idToRescue, discarded, type, userId, app);
        }
        public List<AwardId> Award(int userId, APP app)
        {
            return this.negReward.Award(userId, app);
        }
        public AwardBuy Buy(int rewardId, int userId, int locationId, APP app)
        {
            return this.negReward.Buy(rewardId, userId, locationId, app);
        }
        public AwardId Shot(string qrCode, int userId, APP app)
        {
            return this.negReward.Shot(qrCode, userId, app);
        }
    }
}
