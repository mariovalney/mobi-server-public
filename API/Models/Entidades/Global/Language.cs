﻿using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Global
{
    //[Serializable]
    public class Language : Identity
    {
        private int id;
        private string language;
        private string languageShort;
        /*public int Id
        {
            get { return this.id;}
            set { this.id = value; }
        }*/
        public string Name
        {
            get { return this.language; }
            set { this.language = value; }
        }
        public string Sigla
        {
            get { return languageShort; }
            set { languageShort = value; }
        }
        public int _id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public string _language
        {
            get { return this.language; }
            set { this.language = value; }
        }
        public string _languageShort
        {
            get { return languageShort; }
            set { languageShort = value; }
        }
        public bool _enabled
        {
            get { return true; }
        }
    }
}
