﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Global
{
    public class Image
    {
        private int _id;
        private int _index;
        private string _xdpi;
        private string _hdpi;
        private string _mdpi;
        private string _ldpi;
        private string _mini;
        //[JsonIgnore]
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }
        //[JsonIgnore]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string XDPI
        {
            get { return this._xdpi; }
            set { this._xdpi = value; }
        }
        public string HDPI
        {
            get { return this._hdpi; }
            set { this._hdpi = value; }
        }
        public string MDPI
        {
            get { return this._mdpi; }
            set { this._mdpi = value; }
        }
        public string LDPI
        {
            get { return this._ldpi; }
            set { this._ldpi = value; }
        }
        public string MINI
        {
            get { return _mini; }
            set { _mini = value; }
        }
    }
}