﻿using Boundary.Models.Entidades;
using Global.Entidades;
using System;

namespace API.Models.Entidades
{
    public class FiscalCoupon
    {
        private int _cod;
        private DateTime _at;
        private DateTime _syncAt;
        private FiscalCompany _company;
        private decimal _value;
        private Document _document;
        private int _employee;
        private string _ECF;

        public string ECF
        {
            get { return _ECF; }
            set { _ECF = value; }
        }
        private int _API;

        public int API
        {
            get { return _API; }
            set { _API = value; }
        }
        public int Employee
        {
            get { return _employee; }
            set { _employee = value; }
        }
        private string _qrCode;

        public string QrCode
        {
            get { return _qrCode; }
            set { _qrCode = value; }
        }

        public FiscalCoupon() { }

        public FiscalCompany Company
        {
            get { return _company; }
            set { _company = value; }
        }
        public Document Document
        {
            get { return _document; }
            set { _document = value; }
        }
        public int Cod
        {
            get { return _cod; }
            set { _cod = value; }
        }
        public DateTime At
        {
            get { return _at; }
            set { _at = value; }
        }
        public decimal Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public DateTime SyncAt
        {
            get { return _syncAt; }
            set { _syncAt = value; }
        }
    }
}
