﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLine.Models.Entidades;

namespace API.Models.Entidades
{
    public class GainScore
    {
        public enum GainScoreType
        {
            NOTFOUND = 0,
            PALETA = 1,
            MANAGER = 2,
            OWNER = 3,
            COUPON = 4,
            BAR = 5,
            NFCe = 6
        };
        private GainScoreType scoreType;

        private int netStatus;
        private int plusScore;
        private int userId;
        private int accountId;
        private int locationId;
        private int establishmentId;
        private Int64 hash;
        private string code;
        private DateTime example_at;
        private DateTime example_sync_at;
        private FiscalCoupon coupon;

        public GainScoreType FROM
        {
            get { return this.scoreType; }
            set { this.scoreType = value; }
        }
        public int NetStatus
        {
            get { return netStatus; }
            set { netStatus = value; }
        }
        public int PlusScore
        {
            get { return plusScore; }
            set { plusScore = value; }
        }
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        public Int64 Hash
        {
            get { return hash; }
            set { hash = value; }
        }
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        public int AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }
        public int LocationId
        {
            get { return locationId; }
            set { locationId = value; }
        }
        public int EstablishmentId
        {
            get { return establishmentId; }
            set { establishmentId = value; }
        }
        public DateTime Example_at
        {
            get { return example_at; }
            set { example_at = value; }
        }
        public DateTime Example_sync_at
        {
            get { return example_sync_at; }
            set { example_sync_at = value; }
        }
        public FiscalCoupon Coupon
        {
            get { return coupon; }
            set { coupon = value; }
        }
    }
}
