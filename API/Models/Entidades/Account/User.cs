﻿using Newtonsoft.Json;
using System;

namespace API.Models.Entidades.Account
{
    public class User : Account
    {
        public User() { }
        public User(int userId) { this.Id = userId; }
        private string gender;
        public int     Id { get; set; }
        public string Gender 
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.gender))
                {
                    this.gender = string.Empty;
                }
                return this.gender.Trim();
            }
            set
            {
                this.gender = value;
            }
        }
        public DateTime Birthday { get; set; }
        [JsonIgnore]
        public DateTime SignUpAt { get; set; }
        [JsonIgnore]
        public string Hometown { get; set; }
        [JsonIgnore]
        public string Location { get; set; } 
        [JsonIgnore]
        public string Locale { get; set; } 
        [JsonIgnore]
        public bool BlockedAsUser{get;set;}
    }
}
