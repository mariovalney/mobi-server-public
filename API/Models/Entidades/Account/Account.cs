﻿using Global.Entidades;
using Global.Security;
using Newtonsoft.Json;
using System;

namespace API.Models.Entidades.Account
{
    public class Account
    {
        private string cpf;
        private string fbat;
        private string name;
        private string email;

        public string Name
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.name))
                {
                    this.name = string.Empty;
                }
                return this.name.Trim();
            }
            set
            {
                this.name = value;
            }
        }
        public string Email
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.email))
                {
                    this.email = string.Empty;
                }
                return this.email.Trim();
            }
            set
            {
                this.email = value;
            }
        }
        public long   FacebookId
        {
            get;
            set;
        }
        public string FacebookAccessToken
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.fbat))
                {
                    this.fbat = string.Empty;
                }
                return this.fbat.Trim();
            }
            set
            {
                this.fbat = value;
            }
        }
        [JsonIgnore]
        public long   FacebookAccessExpires { get; set; }
        public string CPF {
            get
            {
                if(string.IsNullOrWhiteSpace(this.cpf))
                {
                    this.cpf = string.Empty;
                }
                return this.cpf.Trim();
            }
            set
            {
                this.cpf = value;
            }
        }
        public string Photo
        {
            get
            {
                if (HasPhoto)
                {
                    return PhotoURL + "?t=" + DateTime.Now.Second * DateTime.Now.Minute * 60 + DateTime.Now.Hour*60 + DateTime.Now.Day *60*60*60;
                }
                else if (this.FacebookId != 0)
                {
                    return "https://graph.facebook.com/" + this.FacebookId + "/picture";
                }
                else
                {
                    return "https://www.exampleclub.com.br/Content/images/user-avatar.png";
                }
            }
        }
        [JsonIgnore]
        public bool HasPhoto { get; set; }
        [JsonIgnore]
        public string PhotoURL { get; set; }
        [JsonIgnore]
        public int IdAccount { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        [JsonIgnore]
        public string PasswordToken { get; set; }
        [JsonIgnore]
        public DateTime ExpirationToken { get; set; }
        [JsonIgnore]
        public bool BlockedAsAccount { get; set; }
        [JsonIgnore]
        public Document Document{ get {return new Document(this.CPF, null);} }
        public void EncodePassword()
        {
            this.Password = CryptoSHA512Hash.CreateHash(this.Email + this.Password);
        }
    }
}
