﻿using API.Models.Entidades.Rating;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class SurveyResponse : WSResponse
    {
        private int _id;
        private bool hasAppreciation;
        private List<SurveyQuestion> _surveyQuestions;
        
        //[JsonIgnore]
        public bool HasAppreciation
        {
            get { return hasAppreciation; }
            set { hasAppreciation = value; }
        }

        //[JsonIgnore]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public List<SurveyQuestion> SurveyQuestions
        {
            get { return _surveyQuestions; }
            set { _surveyQuestions = value; }
        }
    }
}
