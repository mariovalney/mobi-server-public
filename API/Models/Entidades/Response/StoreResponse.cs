﻿using API.Models.Entidades.Display;
using API.Models.Entidades.Global;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class StoreResponse : WSResponse
    {
        private int id;
        private List<Group> group;
        private List<Language> language { get; set; }

        [JsonIgnore]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public List<Group> Group
        {
            get { return group; }
            set { group = value; }
        }
        public List<Language> Languages
        {
            set { this.language = value; }
            get { return  this.language; }
        }
    }
}