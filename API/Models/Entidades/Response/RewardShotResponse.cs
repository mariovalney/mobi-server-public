﻿using API.Models.Entidades.Premium;

namespace API.Models.Entidades.Response
{
    public class RewardShotResponse : WSXResponse
    {
        public AwardId RewardShot { get; set; }
    }
}
