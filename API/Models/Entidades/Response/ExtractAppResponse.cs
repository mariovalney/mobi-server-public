﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class ExtractAppResponse : WSResponse
    {
        public DateTime MinDate;
        public List<ExtractApp> Extract { get; set; }
    }
}
