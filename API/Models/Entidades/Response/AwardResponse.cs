﻿using API.Models.Entidades.Premium;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class AwardResponse : WSResponse
    {
        public List<AwardId> Rewards { get; set; }
    }
}