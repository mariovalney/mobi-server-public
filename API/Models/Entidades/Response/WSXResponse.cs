﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class WSXResponse
    {
        public HttpStatusCode HttpStatus { get; set; }

        //[JsonIgnore]
        public string TitleHead { get; set; }

        //[JsonIgnore]
        public string TitleBody { get; set; }

        //[JsonIgnore]
        public string MessageHead { get; set; }

        //[JsonIgnore]
        public string MessageBody { get; set; }

        public string Title { get {return TitleBody;} }
        public string Message   { get { return MessageBody; } }
    }
}