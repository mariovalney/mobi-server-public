﻿using API.Models.Entidades.Contact;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class OutletResponse : WSXResponse
    {
        public Outlet Location { get; set;}
        //[JsonIgnore]
        public int Id 
        { 
            get { return this.Location.Id; }
        }
        //[JsonIgnore]
        public string Reference 
        { 
            get {return this.Location.Reference; } 
         }
        //[JsonIgnore]
        public string Site { get { return this.Location.Site; } }
        //[JsonIgnore]
        public double Latitude { get { return this.Location.Latitude; } }
        //[JsonIgnore]
        public double Longitude { get { return this.Location.Longitude; } }
        public List<string> Descriptions
        { 
            get { return this.Location.Description; }
        }
        //[JsonIgnore]
        public List<Phone> Phones 
        {
            get { return this.Location.Phone; }
        }
    }
}
