﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class MapResponse : WSResponse
    {
        public List<Map> Map { get{return this.Establishments;} }

        //[JsonIgnore]
        public List<Map> Establishments { get; set; }
    }
}
