﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class HomeResponse : WSXResponse
    {
        public int NewNotifys { get; set; }
        
        //[JsonIgnore]
        public List<Franchise> Establishments { get; set; }
        
        public List<Map> Map { get; set; }
    }
}