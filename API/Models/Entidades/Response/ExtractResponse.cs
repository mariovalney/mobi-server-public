﻿using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class ExtractResponse : WSResponse
    {
        public List<Extract> Months { get; set; }
    }
}
