﻿using System.Net;

namespace API.Models.Entidades.Response
{
    public class WSResponse
    {
        public HttpStatusCode HttpStatus { get; set; }
        private string ttl;
        public string Title 
        { 
            get 
            {
                if (string.IsNullOrWhiteSpace(this.ttl)) { return string.Empty; } 
                else { return this.ttl;}
            }
            set 
            { 
                this.ttl = value;
            }
        }
        public string Message    { get; set; }
    }
}