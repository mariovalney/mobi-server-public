﻿using API.Models.Entidades.Premium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class RescueAwardInfoResponse : WSXResponse
    {
        public AwardPDV Reward {get;set;}
    }
}