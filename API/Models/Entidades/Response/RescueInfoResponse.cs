﻿using API.Models.Entidades.Account;
using API.Models.Entidades.Premium;

namespace API.Models.Entidades.Response
{
    public class RescueInfoResponse : WSResponse
    {
        private AwardPDV reward;
        private User  user;

        public RescueInfoResponse() { }
        public RescueInfoResponse(AwardInfo rescueInfo) 
        {
            this.reward = rescueInfo.Award;
            this.user   = rescueInfo.User;
        }
        
        public User User
        {
            get { return user; }
            set { user = value; }
        }
        public AwardPDV Reward
        {
            get { return reward; }
            set { reward = value; }
        }
    }
}
