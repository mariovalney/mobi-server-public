﻿using API.Models.Entidades.Premium;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class Showcase : WSResponse
    {
        public List<RewardBuy> Rewards { get; set; }
        public int Length { get { return this.Rewards.Count; } }
    }
}