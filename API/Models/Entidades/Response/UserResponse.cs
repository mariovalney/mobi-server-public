﻿using API.Models.Entidades.Account;
using System;
using System.Net;

namespace API.Models.Entidades.Response
{
    public class UserResponse : WSResponse
    {
        public UserResponse(User user) 
        {
            base.HttpStatus = HttpStatusCode.OK;
            this.User = user;
        }
        public User User { get; set; }
        public long facebookAccessExpires
        {
            get { return this.User.FacebookAccessExpires; } 
        } 
        public string FacebookAccessToken
        {
            get 
            {
                if (this.User.FacebookAccessToken == null)
                {
                    this.User.FacebookAccessToken = string.Empty;
                }
                return this.User.FacebookAccessToken; 
            }
        }
        public int Id
        { 
            get
            {
                return this.User.Id;
            }
        }
        public long FacebookId 
        { 
            get 
            { 
                return this.User.FacebookId; 
            } 
        }
        public DateTime Birthday 
        { 
            get 
            { 
                return this.User.Birthday; 
            } 
        }
        public  string CPF
        {
            get
            {
                if (this.User.CPF == null)
                {
                    this.User.CPF = String.Empty;
                }
                return this.User.CPF;
            }
        }
        public string Gender
        {
            get
            {
                if (this.User.Gender == null)
                {
                    this.User.Gender = string.Empty;
                }
                return this.User.Gender;
            }
        }
        public string Name
        {
            get
            {
                if (this.User.Name == null)
                {
                    this.User.Name = string.Empty;
                }
                return this.User.Name;
            }
        }
        public string Email
        {
            get
            {
                if (this.User.Email == null)
                {
                    this.User.Email = string.Empty;
                }
                return this.User.Email;
            }
        }
    }
}
