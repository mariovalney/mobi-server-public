﻿using API.Models.Entidades.Notify;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
   public class NotifyPlacesResponse : WSResponse
   {
       public List<Franchise> Establishments { get; set; }
       public List<Why>       Reasons { get; set; }
   }
}