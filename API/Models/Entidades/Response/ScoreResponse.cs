﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Response
{
    public class ScoreResponse : WSResponse
    {
        public int Score { get; set; }
    }
}