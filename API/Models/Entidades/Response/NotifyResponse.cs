﻿using API.Models.Entidades.Notify;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models.Entidades.Response
{
    public class NotifyResponse : WSResponse
    {
        public  List<Message> Notifys { get; set; }
        public  int  Page             { get; set; }
        public  int  Length           { get; set; }
        
        [JsonIgnore]
        public int Length_Required { get; set; }
        [JsonIgnore]
        public int Num_Of_Messages { get; set; }
        public bool HasNext
        { 
            get 
            {
                if (Num_Of_Messages > Length_Required)
                {
                    int count_of_page = Num_Of_Messages / Length_Required;
                    if ((Num_Of_Messages % Length_Required) > 0)
                    {
                        count_of_page = count_of_page + 1;
                    }
                    if (count_of_page > Page)
                    {
                        return true;
                    }
                    else 
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }  
        }
    }
}