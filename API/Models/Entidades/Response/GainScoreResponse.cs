﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLine.Models.Entidades;

namespace API.Models.Entidades.Response
{
    public class GainScoreResponse : WSXResponse
    {
        public int PlusScore { get; set; }
        
        //[JsonIgnore]
        public Franchise Establishment { get; set; }
        public Map Map { get { return new Map(this.Establishment); } }
    }
}
