﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Contact
{
    public class Address
    {
        private string street;
        private string cep;
        private string uf;
        private string number;
        private string city;
        private string district;
        private string additional;
        
        public string Street
        {
            get {
                if (this.street == null) { this.street = string.Empty; }
                return this.street; }
            set { this.street = value; }
        }
        public string Cep
        {
            get
            {
                if (this.cep == null) { this.cep = string.Empty; } 
                return cep; }
            set { cep = value; }
        }
        public string UF
        {
            get { if (this.uf == null) { this.uf = string.Empty; } return uf; }
            set { uf = value; }
        }
        public string Number
        {
            get { if (this.number == null) { this.number = string.Empty; } return number; }
            set { number = value; }
        }
        public string City
        {
            get { if (this.city == null) { this.city = string.Empty; } return city; }
            set { city = value; }
        }
         public string District
        {
            get { if (this.district == null) { this.district = string.Empty; } return district; }
            set { district = value; }
        }
        public string Additional
        {
            get { if (this.additional == null) { this.additional = string.Empty; } return additional; }
            set { additional = value; }
        }
    }
}
