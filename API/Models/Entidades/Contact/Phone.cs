﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Contact
{
    public class Phone
    {
        public int DDI { get; set; }
        public int DDD { get; set; }
        private string phone { get; set; }
        public string PhoneNumber 
        {   
            get
            {
                if(this.phone == null)
                {
                    this.phone = string.Empty;
                }
                return this.phone;
            }
            set{this.phone = value;}
        }
    }
}
