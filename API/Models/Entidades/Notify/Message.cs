﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Notify
{
    public class Message
    {
        public Int64    Id        { get; set; }
        public string   Title     { get; set; }
        public string   Text      { get; set; }
        public string   From      { get; set; }
        public DateTime SendAt    { get; set; }
        public DateTime ReadAt    { get; set; }
        public bool     Read      { get; set; }
        public string   Name      { get; set; }
        public string   Reference { get; set; }
        public string   LogoUrl   { get; set; }
        public int      UserId    { get; set; }
    }
}
