﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Notify
{
    public class Why
    {
        public int Id { get; set; }
        private string why;

        public string Text
        {
            get 
            {
                if (string.IsNullOrWhiteSpace(this.why))
                {
                    this.why = string.Empty;
                }
                return this.why.Trim(); 
            }
            set { this.why = value; }
        }
    }
}

