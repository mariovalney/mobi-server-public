﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class Award : Reward
    {
        public int IdToRescue { get; set; }
        public string IdToHex { get; set; }
        public string IdtoHex { get{return this.IdToHex;} }
        public string Type { get; set; }
        public DateTime RewardAt { get; set; }
        public DateTime RedeemedAt { get; set; }
        public DateTime ExpirationAt { get; set; }
        public bool Expired { get; set; }
        public bool Redeemed { get; set; }
        public bool Responsed { get; set; }
    }
}
