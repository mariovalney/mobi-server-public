﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class RewardBuy : Reward
    {
        public RewardBuy(Reward reward) 
        {
            base.Description = reward.Description;
            base.Title       = reward.Title;
            base.Image       = reward.Image;
            base.FranchiseId = reward.FranchiseId;
            base.OutletId    = reward.OutletId;
        }
        public int Id { get; set; }
        public int Price { get; set; }
        public int PriceMissing { get; set; }
        public int PDVId { get; set; }
        //[JsonIgnore]
        public string FBSuggestedComment { get; set; }
    }
}
