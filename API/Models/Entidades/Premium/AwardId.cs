﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class AwardId : Award
    {
        public AwardId(Award award) 
        {
            base.Description  = award.Description;
            base.Title        = award.Title;
            base.ExpirationAt = award.ExpirationAt;
            base.RedeemedAt   = award.RedeemedAt;
            base.RewardAt     = award.RewardAt;
            base.Type         = award.Type;
            base.Responsed    = award.Responsed;
            base.Redeemed     = award.Redeemed;
            base.Expired      = award.Expired;
            base.OutletId     = award.OutletId;
            base.FranchiseId  = award.FranchiseId;
            base.IdToRescue   = award.IdToRescue;
            base.IdToHex      = award.IdToHex;
            base.Image        = award.Image;
        }
        public int Id { get; set; }
        public string FBPostSuggested { get { return ""; } }
        public string EstablishmentName { get { return ""; } }
        public string EstablishmentLogoURL { get { return ""; } }
        public long EstablishmentFBPage { get { return 0; } }
        public int LocationId { get { return -99; } }
    }
}
