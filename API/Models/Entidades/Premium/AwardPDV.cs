﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class AwardPDV :Award
    {
        public AwardPDV(Award award) 
        {
            base.Description  = award.Description;
            base.Title        = award.Title;
            base.ExpirationAt = award.ExpirationAt;
            base.RedeemedAt   = award.RedeemedAt;
            base.RewardAt     = award.RewardAt;
            base.Type         = award.Type;
            base.Responsed    = award.Responsed;
            base.Redeemed     = award.Redeemed;
            base.Expired      = award.Expired;
            base.OutletId     = award.OutletId;
            base.FranchiseId  = award.FranchiseId;
            base.IdToRescue   = award.IdToRescue;
            base.IdToHex      = award.IdToHex;
            base.Image        = award.Image;
        }
        public int PDVId { get; set; }
        public string Id { get; set; }
    }
}
