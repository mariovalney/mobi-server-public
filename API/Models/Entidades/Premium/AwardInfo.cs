﻿using API.Models.Entidades.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class AwardInfo
    {
        public AwardInfo() { }
        public User User   { get; set; }
        public AwardPDV Award { get; set; }

        public AwardInfo(User user, AwardPDV award) 
        {
            this.User = user; this.Award = award;
        }
    }
}
