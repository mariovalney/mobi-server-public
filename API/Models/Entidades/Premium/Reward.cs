﻿using API.Models.Entidades.Global;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Premium
{
    public class Reward
    {
        public Image Image { get; set; }
        public string Title       { get; set; }
        public string Description { get; set; }

        //[JsonIgnore]
        public int FranchiseId { get; set; }
        //[JsonIgnore]
        public int OutletId { get; set; }
    }
}
