﻿using API.Models.Entidades.Contact;
using Global.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Geo : Identity
    {
        public Geo() { }
        public Geo(Outlet outlet) 
        {
            this.Id        = outlet.Id;
            this.Reference = outlet.Reference;
            this.Name      = outlet.Reference;
            this.Latitude  = outlet.Latitude;
            this.Longitude = outlet.Longitude;
            this.Distance  = outlet.Distance;
            this.LogoURL   = outlet.LogoURL;
            this.Score     = outlet.Score;
            this.HasReward = outlet.HasReward;
            this.Favorited = outlet.Favorited;
            this.CoverURL  = outlet.CoverURL;
            this.Address   = outlet.Address;
        }
        //[JsonIgnore]
        public string Reference { get; set; }
        public string Name      { get; set; }
        public double Latitude  { get; set; }
        public double Longitude { get; set; }
        public double Distance  { get; set; }
        public int    Score     { get; set; }
        public bool   HasReward { get; set; }
        public string LogoURL   { get; set; }
        public bool   Favorited { get; set; }
        public string CoverURL  { get; set; }
        public Address Address  { get; set; }
    }
}
