﻿using API.Models.Entidades.Global;
using Global.Entidades;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.Models.Entidades.Display
{
    public class Item : Identity
    {
        private string name;
        private string description;
        private bool favorite;
        private string additionalDescription;
        private Image image;
        private Language language;
        private List<Cost> price;
        public bool Favorite
        {
            get { return favorite; }
            set { favorite = value; }
        }
        public string Name
        {
            get
            {
                if (this.name == null)
                {
                    this.name = string.Empty;
                }
                return this.name.Trim();
            }
            set { this.name = value; }
        }
        public string Description
        {
            get
            {
                if (this.description == null)
                {
                    this.description = string.Empty;
                }
                return this.description.Trim();
            }
            set { this.description = value; }
        }
        [JsonIgnore]
        public string AdditionalDescription
        {
            get
            {
                if (this.additionalDescription == null)
                {
                    this.additionalDescription = string.Empty;
                }
                return this.additionalDescription.Trim();
            }
            set { this.additionalDescription = value; }
        }
        public List<Cost> Prices
        {
            get
            {
                if (this.price == null)
                {
                    this.price = new List<Cost>();
                }
                return this.price;
            }
            set { this.price = value; }
        }
        public Image Image
        {
            get { return this.image; }
            set { this.image = value; }
        }
    }
}