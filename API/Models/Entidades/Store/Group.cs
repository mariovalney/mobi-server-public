﻿using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Display
{
    public class Group : Identity
    {
        private string     name;
        private List<Item> itens;
        public string Name
        {
            get 
            {
                if (this.name == null)
                {
                    this.name = string.Empty;
                }
                return this.name.Trim(); 
            }
            set { this.name = value; }
        }
        public List<Item> Itens
        {
            get 
            {
                if (this.itens == null)
                {
                    this.itens = new List<Item>();
                }
                return this.itens; 
            }
            set { this.itens = value; }
        }
    }
}