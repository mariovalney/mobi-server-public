﻿using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Display
{
    public class Cost
    {
        private string size;
        public decimal Price { get; set; }
        public string  Size 
        {
            get 
            {
                if (string.IsNullOrWhiteSpace(this.size))
                {
                    this.size = string.Empty;
                }
                return this.size;
            } 
            set{this.size = value;} 
        }
    }
}