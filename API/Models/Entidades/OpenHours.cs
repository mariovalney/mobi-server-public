﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class OpenHours
    {
        private TimeSpan openHour;
        private TimeSpan closeHour;
        private int weekDay;
        private bool opened;

        public bool Opened
        {
            get { return opened; }
            set { opened = value; }
        }
        public TimeSpan OpenHour
        {
            get { return this.openHour; }
            set { this.openHour = value; }
        }
        public TimeSpan CloseHour
        {
            get { return this.closeHour; }
            set { this.closeHour = value; }
        }
        public int WeekDay
        {
            get { return this.weekDay; }
            set { this.weekDay = value; }
        }
    }
}
