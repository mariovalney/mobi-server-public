﻿using API.Models.Entidades.Account;
using System;

namespace API.Models.Entidades.Rating
{
    public class SurveyAnswer
    {
        private DateTime _dateAnswer;
        private int _scoreAnswer;
        private SurveySelected _surveySelected;
        private User _user;
        public string Question { get; set; }
        public DateTime DateAnswer
        {
            get { return this._dateAnswer; }
            set { this._dateAnswer = value; }
        }
        public int ScoreAnswer
        {
            get { return this._scoreAnswer; }
            set { this._scoreAnswer = value; }
        }
        public SurveySelected SurveySelected
        {
            get { return this._surveySelected; }
            set { this._surveySelected = value; }
        }
        public int Id
        {
            get 
            {
                if (this.SurveySelected == null)
                {
                    SurveySelected = new SurveySelected();
                }
                return this.SurveySelected.Id;
            } 
            set
            {
                if (this.SurveySelected == null)
                {
                    SurveySelected = new SurveySelected();
                }
                this.SurveySelected.Id = value;
            }
        }
        public User User
        {
            get { return this._user; }
            set { this._user = value; }
        }
    }
}