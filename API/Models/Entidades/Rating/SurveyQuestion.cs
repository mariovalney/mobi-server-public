﻿using Newtonsoft.Json;
namespace API.Models.Entidades.Rating
{
    public class SurveyQuestion
    {
        public int Id { get; set; }
        //[JsonIgnore]
        public int Type { get; set; }
        public string Question { get; set; }
        //[JsonIgnore]
        public int IdToAnswer { get; set; }
    }
}
