﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace API.Models.Entidades.Rating
{
    public class Rating
    {
        //[JsonIgnore]
        public int Id {get;set;}
        //[JsonIgnore]
        public bool HasAppreciation { get; set;}

        public List<SurveyQuestion> SurveyQuestions { get; set;}
    }
}