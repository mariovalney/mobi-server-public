﻿using API.Models.Entidades.Account;
using System;
using TimeLine.Models.Entidades;

namespace API.Models.Entidades.Rating
{
    public class Appreciation
    {
        private DateTime appreciationDate;
        private string comment;
        private int nsi;
        private int nps;
        private User user;
        private Outlet establishmentLocation;

        public DateTime AppreciationDate
        {
            get { return this.appreciationDate; }
            set { this.appreciationDate = value; }
        }
        public string Comment
        {
            get { return this.comment; }
            set { this.comment = value; }
        }
        public int NSI
        {
            get { return this.nsi; }
            set { this.nsi = value; }
        }
        public int NPS
        {
            get { return this.nps; }
            set { this.nps = value; }
        }
        public User User
        {
            get { return this.user; }
            set { this.user = value; }
        }
        public int Location 
        {
            get 
            {
                if (this.EstablishmentLocation == null)
                {
                    this.establishmentLocation = new Outlet();
                }
                return this.EstablishmentLocation.Id;
            }
            set 
            {
                if (this.EstablishmentLocation == null)
                {
                    this.establishmentLocation = new Outlet();
                }
                this.EstablishmentLocation.Id = value;
            }
        }
        public Outlet EstablishmentLocation
        {
            get { return this.establishmentLocation; }
            set { this.establishmentLocation = value; }
        }
    }
}