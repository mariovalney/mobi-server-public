﻿using Global.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Lote : Identity
    {
        private int enabled;
        private Franchise franchise;
        public Lote() { }
        public Lote(int id) : base(id) { }
        public Lote(int id, Franchise franchise) : base(id)
        {
            this.franchise = franchise;
        }
        public int Enabled
        {
            get { return this.enabled; }
            set { this.enabled = value; }
        }
        public Franchise Franchise
        {
            get { return this.franchise; }
            set { this.franchise = value; }
        }
    }
}
