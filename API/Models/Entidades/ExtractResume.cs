﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class ExtractResume
    {
        public int  OutletId {get; set; }
        public int  Score {get; set;}
        public bool HasReward {get; set;}
    }
}