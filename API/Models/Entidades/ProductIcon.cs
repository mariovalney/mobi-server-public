﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class ProductIcon
    {
        private string _active;
        private string _disabled;
        private string _highlighted;
        public string Active
        {
            get
            {
                if (this._active == null)
                {
                    this._active = "";
                }
                return this._active;
            }
            set { this._active = value; }
        }
        public string Disabled
        {
            get
            {
                if (this._disabled == null)
                {
                    this._disabled = "";
                }
                return this._disabled;
            }
            set { this._disabled = value; }
        }
        public string Highlighted
        {
            get
            {
                if (this._highlighted == null)
                {
                    this._highlighted = "";
                }
                return this._highlighted;
            }
            set { this._highlighted = value; }
        }
    }

}
