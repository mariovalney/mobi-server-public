﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Extract
    {
        public DateTime DateReference { get; set; }
        public int ExampleWon { get; set; }
        public int ExampleLose { get; set; }
        public int ExampleUsed { get; set; }
        public int ExampleWillLose { get; set; }
    }
}
