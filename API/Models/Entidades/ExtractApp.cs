﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class ExtractApp
    {
        public string Name  { get; set;}
        public string Title { get; set; } 
        public int Scored   { get; set; } 
        public DateTime At  { get; set; }
        public string Description {get; set;}
        public string Type {get; set;}
        public int Qtd { get; set; }
    }
}
