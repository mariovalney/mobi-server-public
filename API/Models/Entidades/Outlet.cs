﻿using API.Models.Entidades.Contact;
using Global.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Outlet : Identity
    {
        private double _distance;
        private bool _favorited;

        public bool Favorited
        {
            get { return _favorited; }
            set { _favorited = value; }
        }
        private int _score;
        private bool _hasReward;
        private string _reference;
        private double _latitude;
        private double _longitude;
        private string _site;
        private long _fbPage;
        private int _establishmentId;
        private bool _hasLiveMusic;
        private bool _hasFumodromo;
        private bool _hasManobrista;
        private bool _hasAirConditioner;
        private bool _hasAddapted;
        private bool _hasOutsideTables;
        private bool _hasParking;
        private bool _hasDelivery;
        private bool _hasWifi;
        private int _menu;
        private int _survey;
        private int _shopping;
        private bool _enabled;

        private List<string> _description;
        private List<Phone> _phone;
        private WiFi _wiFi;
        private ProductIcon _ProductIcon;
        private List<Payment> _payment;
        private Address _address;

        public Address Address
        {
            get 
            {
                if (this._address == null) 
                {
                    this._address = new Address();
                }
                return this._address; 
            }
            set { this._address = value; }
        }
        private List<OpenHours> _hours;
        public List<OpenHours> Hours 
        { 
            get 
            {
                if (this._hours == null) 
                {
                    this._hours = new List<OpenHours>();
                }
                return this._hours;
            }
            set { this._hours = value; }
        }
        public Outlet() { }
        public Outlet(int id) { this.Id = id; }

        public int Menu
        {
            set { this._menu = value; }
            get { return this._menu; }
        }
        public int Survey
        {
            set { this._survey = value; }
            get { return this._survey; }
        }
        public int Shopping
        {
            set { this._shopping = value; }
            get { return this._shopping; }
        }

        public string CoverURL {get;set;}
        public string LogoURL {get;set;}
        //[JsonIgnore]
        public string Reference
        {
            get { return this._reference; }
            set { this._reference = value; }
        }
        public string Name { get { return this.Reference; } set { this.Reference = value; } }
        public double Latitude
        {
            get { return this._latitude; }
            set { this._latitude = value; }
        }
        public double Longitude
        {
            get { return this._longitude; }
            set { this._longitude = value; }
        }
        //[JsonIgnore]
        public double Lat
        {
            get { return this._latitude; }
            set { this._latitude = value; }
        }
        //[JsonIgnore]
        public double Lon
        {
            get { return this._longitude; }
            set { this._longitude = value; }
        }
        public string Site
        {
            get
            {
                if (this._site == null)
                {
                    this._site = "";
                }
                return this._site;
            }
            set { this._site = value; }
        }
        public long FBPage
        {
            get { return this._fbPage; }
            set { this._fbPage = value; }
        }
        //[JsonIgnore]
        public int EstablishmentId
        {
            get { return _establishmentId; }
            set { _establishmentId = value; }
        }

        public bool HasLiveMusic
        {
            get { return this._hasLiveMusic; }
            set { this._hasLiveMusic = value; }
        }
        public bool HasFumodromo
        {
            get { return this._hasFumodromo; }
            set { this._hasFumodromo = value; }
        }
        public bool HasManobrista
        {
            get { return this._hasManobrista; }
            set { this._hasManobrista = value; }
        }
        public bool HasAirConditioner
        {
            get { return this._hasAirConditioner; }
            set { this._hasAirConditioner = value; }
        }
        public bool HasAddapted
        {
            get { return this._hasAddapted; }
            set { this._hasAddapted = value; }
        }
        public bool HasOutsideTables
        {
            get { return this._hasOutsideTables; }
            set { this._hasOutsideTables = value; }
        }
        public bool HasDelivery
        {
            get { return _hasDelivery; }
            set { _hasDelivery = value; }
        }
        public bool HasWifi
        {
            get { return _hasWifi; }
            set { _hasWifi = value; }
        }
        public bool HasParking
        {
            get { return this._hasParking; }
            set { this._hasParking = value; }
        }
        public double Distance
        {
            get
            {
                if (this._latitude == 0.0 || this._longitude == 0.0)
                {
                    return 0;
                }
                return _distance;
            }
            set { _distance = value; }
        }
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }
        public bool HasReward
        {
            get { return _hasReward; }
            set { _hasReward = value; }
        }
        //[JsonIgnore]
        public List<string> Description
        {
            get
            {
                if (this._description == null)
                {
                    this._description = new List<string>();
                }
                return _description;
            }
            set { _description = value; }
        }
        public List<Phone> Phone
        {
            get
            {
                if (this._phone == null)
                {
                    this._phone = new List<Phone>();
                }
                return this._phone;
            }
            set { _phone = value; }
        }
        //[JsonIgnore]
        public List<Payment> Payment
        {
            get
            {
                if (this._payment == null)
                {
                    this._payment = new List<Payment>();
                }
                return _payment;
            }
            set { _payment = value; }
        }
        //[JsonIgnore]
        public WiFi WiFi
        {
            get
            {
                if (this._wiFi == null)
                {
                    this._wiFi = new WiFi();
                }
                return _wiFi;
            }
            set { _wiFi = value; }
        }
        public ProductIcon ProductIcon
        {
            get
            {
                if (this._ProductIcon == null)
                {
                    this._ProductIcon = new ProductIcon();
                }
                return _ProductIcon;
            }
            set { _ProductIcon = value; }
        }
        //[JsonIgnore]
        public bool Enabled
        {
            get { return this._enabled; }
            set { this._enabled = value; }
        }
    }
}