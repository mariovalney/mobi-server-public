﻿using API.Models.Entidades.Response;
using Global.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Map : Identity
    {
        public Map() { }
        public Map(Franchise franchise) 
        {
            this.Id = franchise.Id;
            this.LogoURL = franchise.LogoURL;
            this.Name = franchise.Name;
            this.Geo = new List<Geo>();
            foreach (var outlet in franchise.Locations)
            {
                this.Geo.Add(new Geo(outlet));
            }
        }
        public string Name { get; set; }
        public string LogoURL { get; set; }

        //[JsonIgnore]
        public List<Geo> Locations { get { return this.Geo; } set{this.Geo = value;} }

        public List<Geo> Geo { get; set;}
    }
}
