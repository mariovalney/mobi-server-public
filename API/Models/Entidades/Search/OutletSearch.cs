﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades.Search
{
    public enum OutletSearch
    {
        QrCode = 1,
        Coupon = 2
    }
}
