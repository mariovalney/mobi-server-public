﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models.Entidades
{
    public class Franchise
    {
        public enum EstablishmentType
        {
            NOTDEFINE = -1,
            VAREJO = 1,
            REDE = 2
        };
        private EstablishmentType _establishmentType;
        public EstablishmentType Type
        {
            get { return this._establishmentType; }
            set { this._establishmentType = value; }
        }

        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string LogoURL { get; set; }
        public List<Outlet> Locations { get; set; }
    }
}
