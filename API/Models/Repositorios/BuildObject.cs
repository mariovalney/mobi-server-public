﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using API.Models.Entidades.Global;
using API.Models.Entidades.Notify;
using API.Models.Entidades.Premium;
using BD.IRepositorios;
using Global.Entidades;
using Global.Utils;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device;
using System.Device.Location;
using API.Models.Entidades.Contact;

namespace API.Models.Repositorios
{
    public class BuildObject : Repositorio
    {
        public static T Build<T>(DbDataReader dr, double lon = 0, double lat = 0)
        {
            var type = typeof(T);
            if (type == typeof(Franchise))
            {
                var temp = default(Franchise);
                var franchise = default(Franchise);
                while (dr.Read())
                {
                    temp = LoadFranchise(dr);
                    if (franchise == null)
                    {
                        franchise = temp;
                    }
                    else
                    {
                        franchise.Locations.Add(temp.Locations[0]);
                    }
                }
                return (T)Convert.ChangeType(franchise, type);
            }
            else if (type == typeof(Outlet))
            {
                var outlet = default(Outlet);
                if (dr.Read())
                {
                    outlet = LoadOutlet(dr);
                    outlet.Address = new Address();
                }
                return (T)Convert.ChangeType(outlet, type);
            }
            else if (type == typeof(Lote))
            {
                var lote = default(Lote);
                if (dr.Read())
                {
                    lote = LoadLote(dr);
                }
                return (T)Convert.ChangeType(lote, type);
            }
            else if (type == typeof(Address))
            {
                var address = default(Address);
                if (dr.Read())
                {
                    address = LoadAddress(dr);
                }
                return (T)Convert.ChangeType(address, type);
            }
            else if (type == typeof(List<OpenHours>))
            {
                var hour = new List<OpenHours>();
                while (dr.Read())
                {
                    hour.Add(LoadOpenHour(dr));
                }
                return (T)Convert.ChangeType(hour, type);
            }
            else if (type == typeof(List<Map>))
            {
                var map = new List<Map>();
                var currentMap = default(Map);
                var currentGeo = default(Geo);
                while (dr.Read())
                {
                    currentMap = LoadMap(dr);
                    currentGeo = LoadGeo(dr);
                    if ((int)lat == 0 && (int)lon == 0)
                    {
                        currentGeo.Distance = 0;
                    }
                    else
                    {
                        var sCoord = new GeoCoordinate(lat, lon);
                        var eCoord = new GeoCoordinate(currentGeo.Latitude, currentGeo.Longitude);
                        currentGeo.Distance = (sCoord.GetDistanceTo(eCoord)/1000);
                    }
                    if (map.Find(x => x.Id == currentMap.Id) == null)
                    {
                        currentMap.Locations = new List<Geo>();
                        currentMap.Locations.Add(currentGeo);
                        map.Add(currentMap);
                    }
                    else
                    {
                        map.Find(x => x.Id == currentMap.Id).Locations.Add(currentGeo);
                    }
                }
                return (T)Convert.ChangeType(map, type);
            }
            else if (type == typeof(List<Phone>))
            {
                List<Phone> phone = new List<Phone>();
                while (dr.Read())
                {
                    phone.Add(LoadPhone(dr));
                }
                return (T)Convert.ChangeType(phone, type);
            }
            else if (type == typeof(AwardId))
            {
                AwardId award = null;
                while (dr.Read())
                {
                    award = LoadAwardId(dr);
                    award.Image = LoadImage(dr);
                }
                return (T)Convert.ChangeType(award, type);
            }
            else if (type == typeof(List<AwardId>))
            {
                var award = new List<AwardId>();
                var award_temp = default(AwardId);
                while (dr.Read())
                {
                    award_temp = LoadAwardId(dr);
                    award_temp.Image = LoadImage(dr);
                    if (award.Count == 0)
                    {
                        award.Add(award_temp);
                    }
                    else
                    {
                        if (award.Find(x => x.IdToRescue == award_temp.IdToRescue) == null)
                        {
                            award.Add(award_temp);
                        }
                    }
                }
                return (T)Convert.ChangeType(award, type);
            }
            else if (type == typeof(AwardBuy))
            {
                AwardBuy award = null;
                if (dr.Read())
                {
                    award = LoadAwardBuy(dr);
                    award.Image = LoadImage(dr);
                }
                return (T)Convert.ChangeType(award, type);
            }
            else if (type == typeof(AwardInfo))
            {
                var awardInfo = default(AwardInfo);
                if (dr.Read())
                {
                    awardInfo             = new AwardInfo();
                    awardInfo.Award       = LoadAwardPDV(dr);
                    awardInfo.Award.Image = LoadImage(dr);
                    awardInfo.User        = LoadUser(dr);
                    awardInfo.User.CPF    = LoadDocument(dr).CPF;
                }
                return (T)Convert.ChangeType(awardInfo, type);
            }
            if (type == typeof(List<RewardBuy>))
            {
                List<RewardBuy> rewardBuy = new List<RewardBuy>();
                RewardBuy temp = null;
                while (dr.Read())
                {
                    temp = LoadRewardBuy(dr);
                    if (rewardBuy.Find(p => p.Id == temp.Id) == null)
                    {
                        temp.Image = LoadImage(dr);
                        rewardBuy.Add(temp);
                    }
                }
                return (T)Convert.ChangeType(rewardBuy, type);
            }
            if (type == typeof(List<Extract>))
            {
                var extract = new List<Extract>();
                while (dr.Read())
                {
                    extract.Add(LoadExtract(dr));
                }
                return (T)Convert.ChangeType(extract, type);
            }
            if (type == typeof(List<Message>))
            {
                var notice = new List<Message>();
                while (dr.Read())
                {
                    notice.Add(LoadNotice(dr));
                }
                return (T)Convert.ChangeType(notice, type);
            }
            if (type == typeof(List<ExtractApp>))
            {
                var extract = new List<ExtractApp>();
                while (dr.Read())
                {
                    extract.Add(LoadExtractApp(dr));
                }
                return (T)Convert.ChangeType(extract, type);
            }
            if (type == typeof(List<Why>))
            {
                var why = new List<Why>();
                while (dr.Read())
                {
                    why.Add(LoadWhy(dr));
                }
                return (T)Convert.ChangeType(why, type);
            }
            if (type == typeof(List<Franchise>))
            {
                var temp = default(Franchise);
                var franchise = new List<Franchise>();
                while (dr.Read())
                {
                    temp = LoadHome(dr);
                    if (lat == 0 && lon == 0)
                    {
                        temp.Locations[0].Distance = 0;
                    }
                    if (!dr.IsDBNull("CEP"))
                    {
                        temp.Locations[0].Address = LoadAddress(dr);
                    }
                    if (franchise.Find(x => x.Id == temp.Id) == null)
                    {
                        franchise.Add(temp);
                    }
                    else
                    {
                        franchise.Find(x => x.Id == temp.Id).Locations.Add(temp.Locations[0]);
                    }
                }
                return (T)Convert.ChangeType(franchise, type);
            }
            else if (type == typeof(User))
            {
                var user = default(User);
                if (dr.Read())
                {
                    user = LoadUser(dr);
                    user.CPF = LoadDocument(dr).CPF;
                }
                return (T)Convert.ChangeType(user, type);
            }
            if (type == typeof(Account))
            {
                var account = default(Account);
                if (dr.Read())
                {
                    account     = LoadAccount(dr);
                    account.CPF = LoadDocument(dr).CPF;
                }
                return (T)Convert.ChangeType(account, type);
            }
            else
            {
                throw new Exception("Não foi possível montar o objeto " + type.Name);
            }
        }
        public static Reward LoadReward(DbDataReader dr)
        {
            Reward reward = new Reward();
            if (!dr.IsDBNull("RewardDescription"))
            {
                reward.Description = dr.GetString("RewardDescription");
            }
            if (!dr.IsDBNull("RewardTitle"))
            {
                reward.Title = dr.GetString("RewardTitle");
            }
            if (!dr.IsDBNull("RewardEstablishmentId"))
            {
                reward.FranchiseId = dr.GetInt32("RewardEstablishmentId");
            }
            if (!dr.IsDBNull("RewardLocationId"))
            {
                reward.OutletId = dr.GetInt32("RewardLocationId");
            }
            return reward;
        }
        public static Why LoadWhy(DbDataReader dr)
        {
            Why why = new Why();
            if (!dr.IsDBNull("Id"))
            {
                why.Id = dr.GetInt32("Id");
            }
            if (!dr.IsDBNull("Reason"))
            {
                why.Text = dr.GetString("Reason");
            }
            return why;
        }
        public static RewardBuy LoadRewardBuy(DbDataReader dr)
        {
            RewardBuy rewardBuy = new RewardBuy(LoadReward(dr));
            if (!dr.IsDBNull("RewardId"))
            {
                rewardBuy.Id = dr.GetInt32("RewardId");
            }
            if (!dr.IsDBNull("RewardPrice"))
            {
                rewardBuy.Price = dr.GetInt32("RewardPrice");
            }
            rewardBuy.PDVId = 0;
            rewardBuy.FBSuggestedComment = string.Empty;
            return rewardBuy;
        }

        public static AwardBuy LoadAwardBuy(DbDataReader dr)
        {
            AwardBuy award = new AwardBuy(LoadAward(dr));
            var now = Auxiliar.GetCurrentDateTime();

            if (!dr.IsDBNull("RewardId"))
            {
                award.Id = dr.GetInt32("RewardId");
            }
            if (!dr.IsDBNull("RewardPrice"))
            {
                award.Price = dr.GetInt32("RewardPrice");
            }
            return award;
        }
        public static AwardId LoadAwardId(DbDataReader dr)
        {
            AwardId award = new AwardId(LoadAward(dr));
            if (!dr.IsDBNull("RewardId"))
            {
                award.Id = dr.GetInt32("RewardId");
            }
            return award;
        }

        public static AwardPDV LoadAwardPDV(DbDataReader dr)
        {
            AwardPDV award = new AwardPDV(LoadAward(dr));
            award.Id = award.IdToHex;
            if (!dr.IsDBNull("RewardPDVId"))
            {
                award.PDVId = dr.GetInt32("RewardPDVId");
            }
            return award;
        }
        private static Award LoadAward(DbDataReader dr)
        {
            Award award = new Award();
            var now = Auxiliar.GetCurrentDateTime();
            award.Responsed = false;
            if (!dr.IsDBNull("Type"))
            {
                award.Type = dr.GetString("Type");
            }
            if (!dr.IsDBNull("ResponseAt"))
            {
                award.Responsed = true;
            }
            if (!dr.IsDBNull("RescueAt"))
            {
                award.RedeemedAt = dr.GetDateTime("RescueAt");
            }
            if (!dr.IsDBNull("RewardAt"))
            {
                award.RewardAt = dr.GetDateTime("RewardAt");
            }
            if (!dr.IsDBNull("ExpirationAt"))
            {
                award.ExpirationAt = dr.GetDateTime("ExpirationAt");
            }
            if (!dr.IsDBNull("RewardDescription"))
            {
                award.Description = dr.GetString("RewardDescription");
            }
            if (!dr.IsDBNull("RewardTitle"))
            {
                award.Title = dr.GetString("RewardTitle");
            }
            if (!dr.IsDBNull("RewardEstablishmentId"))
            {
                award.FranchiseId = dr.GetInt32("RewardEstablishmentId");
            }
            if (!dr.IsDBNull("RewardLocationId"))
            {
                award.OutletId = dr.GetInt32("RewardLocationId");
            }
            if (!dr.IsDBNull("IdToRescue"))
            {
                award.IdToRescue = dr.GetInt32("IdToRescue");
                award.IdToHex = Auxiliar.maskReward(award.IdToRescue, award.Type);
            }
            if (award.RedeemedAt != DateTime.MinValue)
            {
                award.Redeemed = true;
            }
            else if (award.ExpirationAt <= now)
            {
                award.Expired = true;
            }

            return award;
        }
        private static Message LoadNotice(DbDataReader dr)
        {
            var notice = new Message();
            if (!dr.IsDBNull("LogoUrl"))
            {
                notice.LogoUrl = dr.GetString("LogoUrl");
            }
            if (!dr.IsDBNull("Id"))
            {
                notice.Id = dr.GetInt64("Id");
            }
            if (!dr.IsDBNull("UserId"))
            {
                notice.UserId = dr.GetInt32("UserId");
            }
            if (!dr.IsDBNull("Send_at"))
            {
                notice.SendAt = dr.GetDateTime("Send_at");
            }
            if (!dr.IsDBNull("Receive_at"))
            {
                notice.ReadAt = dr.GetDateTime("Receive_at");
                notice.Read = true;
            }
            if (!dr.IsDBNull("Name"))
            {
                var Reference = string.Empty;
                notice.Name = dr.GetString("Name");
                if (!dr.IsDBNull("Reference"))
                {
                    Reference = dr.GetString("Reference");
                }
                if (!string.IsNullOrWhiteSpace(Reference))
                {
                    notice.Name = dr.GetString("Name") + " - " + Reference;
                }
                notice.From = notice.Name;
                notice.Reference = notice.Name;
            }
            if (!dr.IsDBNull("Subject"))
            {
                notice.Title = dr.GetString("Subject");
            }
            if (!dr.IsDBNull("Message"))
            {
                notice.Text = dr.GetString("Message");
            }
            return notice;
        }
        private static Image LoadImage(DbDataReader dr)
        {
            Image image = new Image();
            if (!dr.IsDBNull("HDPI"))
            {
                image.HDPI = dr.GetString("HDPI");
            }
            if (!dr.IsDBNull("LDPI"))
            {
                image.LDPI = dr.GetString("LDPI");
            }
            if (!dr.IsDBNull("MDPI"))
            {
                image.MDPI = dr.GetString("MDPI");
            }
            if (!dr.IsDBNull("MINI"))
            {
                image.MINI = dr.GetString("MINI");
            }
            if (!dr.IsDBNull("XDPI"))
            {
                image.XDPI = dr.GetString("XDPI");
            }
            image.Id = 0;
            image.Index = 0;
            return image;
        }
        private static Lote LoadLote(DbDataReader dr)
        {
            Lote lote = new Lote();
            lote.Franchise = new Franchise();
            lote.Franchise.Locations = new List<Outlet>();
            lote.Franchise.Locations.Add(new Outlet());

            if (!dr.IsDBNull("Id"))
            {
                lote.Id = dr.GetInt32("Id");
            }
            if (!dr.IsDBNull("Enabled"))
            {
                lote.Enabled = dr.GetInt32("Enabled");
            }
            if (!dr.IsDBNull("EstablishmentId"))
            {
                lote.Franchise.Id = dr.GetInt32("EstablishmentId");
                lote.Franchise.Locations[0].EstablishmentId = lote.Franchise.Id;
            }
            if (!dr.IsDBNull("Name"))
            {
                lote.Franchise.Name = dr.GetString("Name");
            }
            if (!dr.IsDBNull("LogoURL"))
            {
                lote.Franchise.LogoURL = dr.GetString("LogoURL");
            }
            if (!dr.IsDBNull("LocationId"))
            {
                lote.Franchise.Locations[0].Id = dr.GetInt32("LocationId");
            }
            if (!dr.IsDBNull("Reference"))
            {
                lote.Franchise.Locations[0].Reference = dr.GetString("Reference");
            }
            return lote;
        }

        private static Extract LoadExtract(DbDataReader dr)
        {
            Extract extract = new Extract();
            if (!dr.IsDBNull("DateReference"))
            {
                extract.DateReference = dr.GetDateTime("DateReference");
            }
            if (!dr.IsDBNull("Examplewon"))
            {
                extract.ExampleWon = dr.GetInt32("Examplewon");
            }
            if (!dr.IsDBNull("ExampleUsed"))
            {
                extract.ExampleUsed = dr.GetInt32("ExampleUsed");
            }
            if (!dr.IsDBNull("ExampleLost"))
            {
                extract.ExampleLose = dr.GetInt32("ExampleLost");
            }
            if (!dr.IsDBNull("ExampleWillLost"))
            {
                extract.ExampleWillLose = dr.GetInt32("ExampleWillLost");
            }
            return extract;
        }
        private static Account LoadAccount(DbDataReader dr)
        {
            var account = new Account();
            if (!dr.IsDBNull("AccountId"))
            {
                account.IdAccount = dr.GetInt32("AccountId");
            }
            if (!dr.IsDBNull("AccountBlocked"))
            {
                account.BlockedAsAccount = dr.GetBoolean("AccountBlocked");
            }
            if (!dr.IsDBNull("AccountName"))
            {
                account.Name = dr.GetString("AccountName");
            }
            if (!dr.IsDBNull("AccountEmail"))
            {
                account.Email = dr.GetString("AccountEmail");
            }
            if (!dr.IsDBNull("AccountPassword"))
            {
                account.Password = dr.GetString("AccountPassword");
            }
            if (!dr.IsDBNull("AccountPasswordToken"))
            {
                account.PasswordToken = dr.GetString("AccountPasswordToken");
            }
            if (!dr.IsDBNull("AccountExpirationToken"))
            {
                account.ExpirationToken = dr.GetDateTime("AccountExpirationToken");
            }
            if (!dr.IsDBNull("AccountPhoto"))
            {
                account.HasPhoto = true;
                account.PhotoURL = dr.GetString("AccountPhoto");
            }
            return account;
        }
        private static User LoadUser(DbDataReader dr)
        {
            var user = new User();
            if (!dr.IsDBNull("AccountBlocked"))
            {
                user.BlockedAsAccount = dr.GetBoolean("AccountBlocked");
            }
            if (!dr.IsDBNull("UserBlocked"))
            {
                user.BlockedAsUser = dr.GetBoolean("UserBlocked");
            }
            if (!dr.IsDBNull("AccountSignUpAt"))
            {
                user.SignUpAt = dr.GetDateTime("AccountSignUpAt");
            }
            if (!dr.IsDBNull("AccountPassword"))
            {
                user.Password = dr.GetString("AccountPassword");
            }
            if (!dr.IsDBNull("AccountPasswordToken"))
            {
                user.PasswordToken = dr.GetString("AccountPasswordToken");
            }
            if (!dr.IsDBNull("AccountExpirationToken"))
            {
                user.ExpirationToken = dr.GetDateTime("AccountExpirationToken");
            }
            if (!dr.IsDBNull("UserBirthday"))
            {
                user.Birthday = dr.GetDateTime("UserBirthday");
            }
            if (!dr.IsDBNull("UserGender"))
            {
                user.Gender = dr.GetString("UserGender");
            }
            if (!dr.IsDBNull("UserFacebookId"))
            {
                user.FacebookId = dr.GetInt64("UserFacebookId");
            }
            if (!dr.IsDBNull("UserFBAToken"))
            {
                user.FacebookAccessToken = dr.GetString("UserFBAToken");
            }
            if (!dr.IsDBNull("UserFBAExpires"))
            {
                user.FacebookAccessExpires = dr.GetInt64("UserFBAExpires");
            }
            if (!dr.IsDBNull("UserId"))
            {
                user.Id = dr.GetInt32("UserId");
            }
            if (!dr.IsDBNull("AccountId"))
            {
                user.IdAccount = dr.GetInt32("AccountId");
            }
            if (!dr.IsDBNull("AccountName"))
            {
                user.Name = dr.GetString("AccountName");
            }
            if (!dr.IsDBNull("AccountEmail"))
            {
                user.Email = dr.GetString("AccountEmail");
            }
            if (!dr.IsDBNull("AccountPhoto"))
            {
                user.HasPhoto = true;
                user.PhotoURL = dr.GetString("AccountPhoto");
            }
            return user;
        }
        private static Document LoadDocument(DbDataReader dr)
        {
            var document = new Document();
            if (!dr.IsDBNull("DocumentId"))
            {
                document.Id = dr.GetInt32("DocumentId");
            }
            if (!dr.IsDBNull("CPF"))
            {
                document.CPF = dr.GetString("CPF");
            }
            return document;
        }
        private static Franchise LoadHome(DbDataReader dr)
        {
            Franchise franchise = new Franchise();
            franchise.Locations = new List<Outlet>();
            franchise.Locations.Add(new Outlet());
            franchise.Locations[0].HasReward = false;
            if (!dr.IsDBNull("EstablishmentId"))
            {
                franchise.Id = dr.GetInt32("EstablishmentId");
            }
            if (!dr.IsDBNull("EstablishmentEnabled"))
            {
                franchise.Enabled = dr.GetBoolean("EstablishmentEnabled");
            }
            if (!dr.IsDBNull("EstablishmentName"))
            {
                franchise.Name = dr.GetString("EstablishmentName");
            }
            if (!dr.IsDBNull("EstablishmentLogoURL"))
            {
                franchise.LogoURL = dr.GetString("EstablishmentLogoURL");
            }
            if (!dr.IsDBNull("LocationId"))
            {
                franchise.Locations[0].Id = dr.GetInt32("LocationId");
            }
            if (!dr.IsDBNull("EstablishmentId"))
            {
                franchise.Locations[0].EstablishmentId = dr.GetInt32("EstablishmentId");
            }
            if (!dr.IsDBNull("LocationReference"))
            {
                franchise.Locations[0].Reference = dr.GetString("LocationReference");
            }
            if (!dr.IsDBNull("LocationLatitude"))
            {
                franchise.Locations[0].Latitude = dr.GetDouble("LocationLatitude");
            }
            if (!dr.IsDBNull("LocationLongitude"))
            {
                franchise.Locations[0].Longitude = dr.GetDouble("LocationLongitude");
            }
            if (!dr.IsDBNull("Score"))
            {
                franchise.Locations[0].Score = dr.GetInt32("Score");
            }
            if (!dr.IsDBNull("Distance"))
            {
                franchise.Locations[0].Distance = dr.GetDouble("Distance");
            }
            if (!dr.IsDBNull("Site"))
            {
                franchise.Locations[0].Site = dr.GetString("Site");
            }
            if (!dr.IsDBNull("FbPage"))
            {
                franchise.Locations[0].FBPage = dr.GetInt64("FbPage");
            }
            if (!dr.IsDBNull("LocationEnabled"))
            {
                franchise.Locations[0].Enabled = dr.GetBoolean("LocationEnabled");
            }
            if (!dr.IsDBNull("LocationLogoURL"))
            {
                franchise.Locations[0].LogoURL = dr.GetString("LocationLogoURL");
            }
            if (!dr.IsDBNull("CoverURL"))
            {
                franchise.Locations[0].CoverURL = dr.GetString("CoverURL");
            }
            if (!dr.IsDBNull("HasLiveMusic"))
            {
                franchise.Locations[0].HasLiveMusic = dr.GetBoolean("HasLiveMusic");
            }
            if (!dr.IsDBNull("HasFumodromo"))
            {
                franchise.Locations[0].HasFumodromo = dr.GetBoolean("HasFumodromo");
            }
            if (!dr.IsDBNull("HasManobrista"))
            {
                franchise.Locations[0].HasAirConditioner = dr.GetBoolean("HasAirConditioner");
            }
            if (!dr.IsDBNull("HasManobrista"))
            {
                franchise.Locations[0].HasAddapted = dr.GetBoolean("HasAddapted");
            }
            if (!dr.IsDBNull("HasOutsideTables"))
            {
                franchise.Locations[0].HasOutsideTables = dr.GetBoolean("HasOutsideTables");
            }
            if (!dr.IsDBNull("HasParking"))
            {
                franchise.Locations[0].HasParking = dr.GetBoolean("HasParking");
            }
            if (!dr.IsDBNull("HasDelivery"))
            {
                franchise.Locations[0].HasDelivery = dr.GetBoolean("HasDelivery");
            }
            if (!dr.IsDBNull("HasWifi"))
            {
                franchise.Locations[0].HasWifi = dr.GetBoolean("HasWifi");
            }
            if (!dr.IsDBNull("Menu"))
            {
                franchise.Locations[0].Menu = dr.GetInt32("Menu");
            }
            if (!dr.IsDBNull("Survey"))
            {
                franchise.Locations[0].Survey = dr.GetInt32("Survey");
            }
            if (!dr.IsDBNull("Shopping"))
            {
                franchise.Locations[0].Shopping = dr.GetInt32("Shopping");
            }
            if (!dr.IsDBNull("Favorited"))
            {
                franchise.Locations[0].Favorited = dr.GetBoolean("Favorited");
            }
            return franchise;
        }
        private static Franchise LoadFranchise(DbDataReader dr)
        {
            var franchise = new Franchise();
            franchise.Locations = new List<Outlet>();
            franchise.Locations.Add(new Outlet());

            if (!dr.IsDBNull("Name"))
            {
                franchise.Name = dr.GetString("Name");
            }
            if (!dr.IsDBNull("EstablishmentEnabled"))
            {
                franchise.Enabled = dr.GetBoolean("EstablishmentEnabled");
            }
            if (!dr.IsDBNull("Enabled"))
            {
                franchise.Locations[0].Enabled = dr.GetBoolean("Enabled");
            }
            if (!dr.IsDBNull("EstablishmentLogoURL"))
            {
                franchise.LogoURL = dr.GetString("EstablishmentLogoURL");
            }
            if (!dr.IsDBNull("Id"))
            {
                franchise.Locations[0].Id = dr.GetInt32("Id");
            }
            if (!dr.IsDBNull("EstablishmentId"))
            {
                franchise.Id = dr.GetInt32("EstablishmentId");
                franchise.Locations[0].EstablishmentId = dr.GetInt32("EstablishmentId");
            }
            if (!dr.IsDBNull("Latitude"))
            {
                franchise.Locations[0].Latitude = dr.GetDouble("Latitude");
            }
            if (!dr.IsDBNull("Longitude"))
            {
                franchise.Locations[0].Longitude = dr.GetDouble("Longitude");
            }
            if (!dr.IsDBNull("Reference"))
            {
                franchise.Locations[0].Reference = dr.GetString("Reference");
            }
            if (!dr.IsDBNull("FBPage"))
            {
                franchise.Locations[0].FBPage = dr.GetInt64("FBPage");
            }
            if (!dr.IsDBNull("Site"))
            {
                franchise.Locations[0].Site = dr.GetString("Site");
            }
            if (!dr.IsDBNull("LogoURL"))
            {
                franchise.Locations[0].LogoURL = dr.GetString("LogoURL");
            }
            if (!dr.IsDBNull("CoverURL"))
            {
                franchise.Locations[0].CoverURL = dr.GetString("CoverURL");
            }
            if (!dr.IsDBNull("Survey"))
            {
                franchise.Locations[0].Survey = dr.GetInt32("Survey");
            }
            if (!dr.IsDBNull("Shopping"))
            {
                franchise.Locations[0].Shopping = dr.GetInt32("Shopping");
            }
            if (!dr.IsDBNull("Menu"))
            {
                franchise.Locations[0].Menu = dr.GetInt32("Menu");
            }
            if (!dr.IsDBNull("HasParking"))
            {
                franchise.Locations[0].HasParking = dr.GetBoolean("HasParking");
            }
            if (!dr.IsDBNull("HasLiveMusic"))
            {
                franchise.Locations[0].HasLiveMusic = dr.GetBoolean("HasLiveMusic");
            }
            if (!dr.IsDBNull("HasFumodromo"))
            {
                franchise.Locations[0].HasFumodromo = dr.GetBoolean("HasFumodromo");
            }
            if (!dr.IsDBNull("HasManobrista"))
            {
                franchise.Locations[0].HasManobrista = dr.GetBoolean("HasManobrista");
            }
            if (!dr.IsDBNull("HasDelivery"))
            {
                franchise.Locations[0].HasDelivery = dr.GetBoolean("HasDelivery");
            }
            if (!dr.IsDBNull("HasAirConditioner"))
            {
                franchise.Locations[0].HasAirConditioner = dr.GetBoolean("HasAirConditioner");
            }
            if (!dr.IsDBNull("HasAddapted"))
            {
                franchise.Locations[0].HasAddapted = dr.GetBoolean("HasAddapted");
            }
            if (!dr.IsDBNull("HasOutsideTables"))
            {
                franchise.Locations[0].HasOutsideTables = dr.GetBoolean("HasOutsideTables");
            }
            if (!dr.IsDBNull("HasWifi"))
            {
                franchise.Locations[0].HasWifi = dr.GetBoolean("HasWifi");
            }
            return franchise;
        }
        private static OpenHours LoadOpenHour(DbDataReader dr)
        {
            var openHour = new OpenHours();
            if (!dr.IsDBNull("Open"))
            {
                openHour.OpenHour = dr.GetTime("Open");
            }
            if (!dr.IsDBNull("Close"))
            {
                openHour.CloseHour = dr.GetTime("Close");
            }
            if (!dr.IsDBNull("WeekDay"))
            {
                openHour.WeekDay = dr.GetInt32("WeekDay");
            }
            if (!dr.IsDBNull("Opened"))
            {
                openHour.Opened = dr.GetBoolean("Opened");
            }
            return openHour;
        }
        private static Outlet LoadOutlet(DbDataReader dr)
        {
            Outlet outlet = new Outlet();
            outlet.ProductIcon = new ProductIcon();
            outlet.Payment = new List<Payment>();

            if (!dr.IsDBNull("Favorited"))
            {
                outlet.Favorited = dr.GetBoolean("Favorited");
            }
            if (!dr.IsDBNull("ProductOn"))
            {
                outlet.ProductIcon.Active = dr.GetString("ProductOn");
            }
            if (!dr.IsDBNull("ProductOff"))
            {
                outlet.ProductIcon.Disabled = dr.GetString("ProductOff");
            }
            if (!dr.IsDBNull("ProductClick"))
            {
                outlet.ProductIcon.Highlighted = dr.GetString("ProductClick");
            }
            if (!dr.IsDBNull("Id"))
            {
                outlet.Id = dr.GetInt32("Id");
            }
            if (!dr.IsDBNull("EstablishmentId"))
            {
                outlet.EstablishmentId = dr.GetInt32("EstablishmentId");
            }
            if (!dr.IsDBNull("Latitude"))
            {
                outlet.Latitude = dr.GetDouble("Latitude");
            }
            if (!dr.IsDBNull("Longitude"))
            {
                outlet.Longitude = dr.GetDouble("Longitude");
            }
            if (!dr.IsDBNull("Reference"))
            {
                outlet.Reference = dr.GetString("Reference");
            }
            if (!dr.IsDBNull("FBPage"))
            {
                outlet.FBPage = dr.GetInt64("FBPage");
            }
            if (!dr.IsDBNull("Site"))
            {
                outlet.Site = dr.GetString("Site");
            }
            if (!dr.IsDBNull("LogoURL"))
            {
                outlet.LogoURL = dr.GetString("LogoURL");
            }
            if (!dr.IsDBNull("CoverURL"))
            {
                outlet.CoverURL = dr.GetString("CoverURL");
            }
            if (!dr.IsDBNull("Survey"))
            {
                outlet.Survey = dr.GetInt32("Survey");
            }
            if (!dr.IsDBNull("Shopping"))
            {
                outlet.Shopping = dr.GetInt32("Shopping");
            }
            if (!dr.IsDBNull("Menu"))
            {
                outlet.Menu = dr.GetInt32("Menu");
            }
            if (!dr.IsDBNull("HasParking"))
            {
                outlet.HasParking = dr.GetBoolean("HasParking");
            }
            if (!dr.IsDBNull("HasLiveMusic"))
            {
                outlet.HasLiveMusic = dr.GetBoolean("HasLiveMusic");
            }
            if (!dr.IsDBNull("HasFumodromo"))
            {
                outlet.HasFumodromo = dr.GetBoolean("HasFumodromo");
            }
            if (!dr.IsDBNull("HasManobrista"))
            {
                outlet.HasManobrista = dr.GetBoolean("HasManobrista");
            }
            if (!dr.IsDBNull("HasDelivery"))
            {
                outlet.HasDelivery = dr.GetBoolean("HasDelivery");
            }
            if (!dr.IsDBNull("HasAirConditioner"))
            {
                outlet.HasAirConditioner = dr.GetBoolean("HasAirConditioner");
            }
            if (!dr.IsDBNull("HasAddapted"))
            {
                outlet.HasAddapted = dr.GetBoolean("HasAddapted");
            }
            if (!dr.IsDBNull("HasOutsideTables"))
            {
                outlet.HasOutsideTables = dr.GetBoolean("HasOutsideTables");
            }
            return outlet;
        }
        private static Geo LoadGeo(DbDataReader dr)
        {
            var geo = new Geo();
            if (!dr.IsDBNull("LocationId"))
            {
                geo.Id = dr.GetInt32("LocationId");
            }
            if (!dr.IsDBNull("Reference"))
            {
                geo.Reference = dr.GetString("Reference");
                geo.Name = geo.Reference;
            }
            if (!dr.IsDBNull("LocationLogoURL"))
            {
                geo.LogoURL = dr.GetString("LocationLogoURL");
            }
            if (!dr.IsDBNull("Latitude"))
            {
                geo.Latitude = dr.GetDouble("Latitude");
            }
            if (!dr.IsDBNull("Longitude"))
            {
                geo.Longitude = dr.GetDouble("Longitude");
            }
            if (!dr.IsDBNull("Favorited"))
            {
                geo.Favorited = dr.GetBoolean("Favorited");
            }
            if (!dr.IsDBNull("CoverURL"))
            {
                geo.CoverURL = dr.GetString("CoverURL");
            }
            return geo;
        }
        private static Map LoadMap(DbDataReader dr)
        {
            var map = new Map();
            if (!dr.IsDBNull("EstablishmentId"))
            {
                map.Id = dr.GetInt32("EstablishmentId");
            }
            if (!dr.IsDBNull("Name"))
            {
                map.Name = dr.GetString("Name");
            }
            if (!dr.IsDBNull("LogoURL"))
            {
                map.LogoURL = dr.GetString("LogoURL");
            }
            return map;
        }
        private static Phone LoadPhone(DbDataReader dr)
        {
            var phone = new Phone();
            if (!dr.IsDBNull("DDD"))
            {
                phone.DDD = dr.GetInt32("DDD");
            }
            if (!dr.IsDBNull("DDI"))
            {
                phone.DDI = dr.GetInt32("DDI");
            }
            if (!dr.IsDBNull("Phone"))
            {
                phone.PhoneNumber = dr.GetString("Phone");
            }
            return phone;
        }
        private static Address LoadAddress(DbDataReader dr)
        {
            var add = new Address();
            if (!dr.IsDBNull("City"))
            {
                add.City = dr.GetString("City");
            }
            if (!dr.IsDBNull("Street"))
            {
                add.Street = dr.GetString("Street");
            }
            if (!dr.IsDBNull("District"))
            {
                add.District = dr.GetString("District");
            }
            if (!dr.IsDBNull("CEP"))
            {
                add.Cep = dr.GetString("CEP");
            }
            if (!dr.IsDBNull("Number"))
            {
                add.Number = dr.GetString("Number");
            }
            if (!dr.IsDBNull("UF"))
            {
                add.UF = dr.GetString("UF");
            }
            if (!dr.IsDBNull("Additional"))
            {
                add.Additional = dr.GetString("Additional");
            }
            return add;
        }
        public static ExtractApp LoadExtractApp(DbDataReader dr)
        {
            var ext = new ExtractApp();
            if (!dr.IsDBNull("Description"))
            {
                ext.Description = dr.GetString("Description");
            }
            if (!dr.IsDBNull("Name"))
            {
                ext.Name = dr.GetString("Name");
            }
            if (!dr.IsDBNull("Type"))
            {
                ext.Type = dr.GetString("Type");
            }
            if (!dr.IsDBNull("Title"))
            {
                ext.Title = dr.GetString("Title");
            }
            if (!dr.IsDBNull("Qtd"))
            {
                ext.Qtd = dr.GetInt32("Qtd");
            }
            if (!dr.IsDBNull("Scored"))
            {
                ext.Scored = dr.GetInt32("Scored");
            }
            if (!dr.IsDBNull("At"))
            {
                ext.At = dr.GetDateTime("At");
            }
            return ext;
        }
    }
}
