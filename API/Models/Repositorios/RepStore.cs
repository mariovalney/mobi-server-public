﻿using API.Models.Entidades.Display;
using API.Models.Entidades.Global;
using BD.IRepositorios;
using Global.Utils;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace API.Models.Repositorios
{
    public class RepStore : Repositorio
    {
        public void Favorite(int idItem, int idUser) 
        { 
            DatabaseParameter[] parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, idUser);
            parameters[1] = dao.CreateDatabaseParameter("v_store_item_translated_id", DatabaseType.Int32, idItem);
            parameters[2] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            dao.RunProcedure("SP_StoreItem_Favorite", parameters);
        }
        public void Viewed(int idLocation, int idUser) 
        { 
            DatabaseParameter[] parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, idUser);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, idLocation);
            parameters[2] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            dao.RunProcedure("SP_Store_See", parameters);
        }
        public List<Language> Language(int locationId)
        {
            List<Language> language = new List<Language>();
            DatabaseParameter[] parameters = new DatabaseParameter[1];
            parameters[0] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, locationId);
            DbDataReader dr = dao.RunSelectProcedure("FN_WS_Language_Select_Location", parameters);
            while (dr.Read())
            {
                language.Add(LoadLanguage(dr));
            }
            return language;
        }
        private Language LoadLanguage(DbDataReader dr)
        {
            Language language = new Language();
            if (!dr.IsDBNull("Id"))
            {
                language.Id = dr.GetInt32("Id");
            }
            if (!dr.IsDBNull("Language"))
            {
                language.Name = dr.GetString("Language");
            }
            if (!dr.IsDBNull("ShortName"))
            {
                language.Sigla = dr.GetString("ShortName");
            }
            return language;
        }
        public List<Group> Store(int idLocation, int idLanguage, int idUser)
        {
            var list_of_group = new List<Group>();
            var list_of_item  = new List<Item>();
            var current_group = default(Group);
            var group = default(Group);
            var current_item = default(Item);
            
            var parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, idLocation);
            parameters[1] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, idUser);
            parameters[2] = dao.CreateDatabaseParameter("v_language_id", DatabaseType.Int32, idLanguage);
            DbDataReader dr = dao.RunSelectProcedure("FN_WS_StoreGroup_Select_Location", parameters);
            while (dr.Read())
            {
                group = LoadStore(dr);
                current_group = list_of_group.Find(x => x.Id == group.Id);
                if (current_group == null)
                {
                    list_of_group.Add(group);
                }
                else
                {
                    current_item = current_group.Itens.Find(x => x.Id == group.Itens[0].Id);
                    if (current_item == null)
                    {
                        current_group.Itens.Add(group.Itens[0]);
                    }
                    else
                    {
                        current_item.Prices.Add(group.Itens[0].Prices[0]);
                    }
                }
            }
            return list_of_group;
        }
        private Group LoadStore(DbDataReader dr)
        {
            var group = new Group();
            var item  = new Item();
            item.Prices = new List<Cost>();
            var image = new Image();
            var list_of_item = new List<Item>();

            if (!dr.IsDBNull("StoreGroupId"))
            {
                group.Id = dr.GetInt32("StoreGroupId");
            }
            if (!dr.IsDBNull("StoreGroupName"))
            {
                group.Name = dr.GetString("StoreGroupName");
            }
            if (!dr.IsDBNull("StoreItemid"))
            {
                item.Id = dr.GetInt32("StoreItemid");
            }
            if (!dr.IsDBNull("StoreItemName"))
            {
                item.Name = dr.GetString("StoreItemName");
            }
            if (!dr.IsDBNull("StoreItemDescription"))
            {
                item.Description = dr.GetString("StoreItemDescription");
            }
            if (!dr.IsDBNull("Favorited"))
            {
                item.Favorite = true;
            }
            if (!dr.IsDBNull("Price"))
            {
                item.Prices.Add(new Cost
                {
                    Price = dr.GetDecimal(dr.GetOrdinal("Price"))
                });
                if (!dr.IsDBNull("Size"))
                {
                    item.Prices[0].Size = dr.GetString("Size");
                }
            }
            if (!dr.IsDBNull("MINI"))
            {
                image.MINI = dr.GetString("MINI");
            }
            if (!dr.IsDBNull("LDPI"))
            {
                image.LDPI = dr.GetString("LDPI");
            }
            if (!dr.IsDBNull("MDPI"))
            {
                image.MDPI = dr.GetString("MDPI");
            }
            if (!dr.IsDBNull("MINI"))
            {
                image.HDPI = dr.GetString("HDPI");
            }
            if (!dr.IsDBNull("XDPI"))
            {
                image.XDPI = dr.GetString("XDPI");
            }
            item.Image = image;
            list_of_item.Add(item);
            group.Itens = list_of_item;
            return group;
        }
    }
}
