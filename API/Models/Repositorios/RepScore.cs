﻿using API.Models.Entidades;
using API.Models.Entidades.Account;
using BD.IRepositorios;
using Boundary.Models.Entidades;
using Global.Utils;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace API.Models.Repositorios
{
    public class RepScore : Repositorio
    {
        public Lote Lote(Lote lote)
        {
            var parameters = new DatabaseParameter[1];
            parameters[0] = dao.CreateDatabaseParameter("v_lote_id", DatabaseType.Int32, lote.Id);
            var dr = dao.RunSelectProcedure("FN_Lote_Id", parameters);
            return BuildObject.Build<Lote>(dr);
        }
        public List<string> Description(Outlet outlet)
        {
            var description = new List<string>();
            var parameters = new DatabaseParameter[2];
            parameters[0] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, outlet.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_not_status", DatabaseType.Int32 , 0);
            var dr = dao.RunSelectProcedure("FN_Description_Location", parameters);
            while (dr.Read())
            {
                description.Add(dr.GetString("DescriptionDescription"));
            }
            return description;
        }
        public int Score(int userId, int locationId, APP app)
        {
            var parameters = new DatabaseParameter[4];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, userId);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, locationId);
            parameters[2] = dao.CreateDatabaseParameter("v_app_id", DatabaseType.Int32, app.Id);
            parameters[3] = dao.CreateDatabaseParameterOutput("v_examples", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction("SP_Examples_Location_User", parameters));
        }
        public List<Extract> Extract(int userId, int locationId, APP app)
        {
            var parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, userId);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, locationId);
            parameters[2] = dao.CreateDatabaseParameter("v_current_date", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            var dr = dao.RunSelectProcedure("FN_Extract_User_Location", parameters);
            return BuildObject.Build<List<Extract>>(dr);
        }
        public List<ExtractApp> ExtractApp(User user, APP app, DateTime selectedDate)
        {
            var parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_hash_id", DatabaseType.Int32, app.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user.Id);
            parameters[2] = dao.CreateDatabaseParameter("v_select_date", DatabaseType.DateTime, selectedDate);
            var dr = dao.RunSelectProcedure("FN_Extract_App", parameters);
            return BuildObject.Build<List<ExtractApp>>(dr);
        }
        public DateTime ExtractMinDate(User user, APP app)
        {
            var parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_hash_id", DatabaseType.Int32, app.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user.Id);
            parameters[2] = dao.CreateDatabaseParameterOutput("v_min_datetime", DatabaseType.DateTime);
            return (DateTime)dao.RunFunction("SP_Extract_MinDate", parameters);
        }
        public List<ExtractResume> Resume(int idUser)
        {
            List<ExtractResume> locations = new List<ExtractResume>();
            String command = "FN_Club_User";
            DatabaseParameter[] parameters = new DatabaseParameter[2];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, idUser);
            parameters[1] = dao.CreateDatabaseParameter("v_datetime", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            DbDataReader dr = dao.RunSelectProcedure(command, parameters);

            while (dr.Read())
            {
                locations.Add(LoadScoreEstablishmentWS(dr));
            }
            return locations;
        }
        public List<ExtractResume> Resume(int user_id, int franchise_id)
        {
            List<ExtractResume> locations = new List<ExtractResume>();
            String command = "FN_Club_User_Franchise";
            DatabaseParameter[] parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user_id);
            parameters[1] = dao.CreateDatabaseParameter("v_franchise_id", DatabaseType.Int32, franchise_id);
            parameters[2] = dao.CreateDatabaseParameter("v_datetime", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            DbDataReader dr = dao.RunSelectProcedure(command, parameters);

            while (dr.Read())
            {
                locations.Add(LoadScoreEstablishmentWS(dr));
            }
            return locations;
        }
        public ExtractResume Resume(User user, Outlet outlet)
        {
            var locations = new ExtractResume();
            String command = "FN_Club_User_Outlet";
            DatabaseParameter[] parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_outlet_id", DatabaseType.Int32, outlet.Id);
            parameters[2] = dao.CreateDatabaseParameter("v_datetime", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            DbDataReader dr = dao.RunSelectProcedure(command, parameters);

            while (dr.Read())
            {
                locations = LoadScoreEstablishmentWS(dr);
            }
            return locations;
        }
        private ExtractResume LoadScoreEstablishmentWS(DbDataReader dr)
        {
            ExtractResume locationWS = new ExtractResume();
            int price;

            if (!dr.IsDBNull("LocationId"))
            {
                locationWS.OutletId = dr.GetInt32("LocationId");
            }
            if (!dr.IsDBNull("Score"))
            {
                locationWS.Score = dr.GetInt32("Score");
            }
            if (!dr.IsDBNull("MinPrice"))
            {
                price = dr.GetInt32("MinPrice");
                if (locationWS.Score >= price && price != 0)
                {
                    locationWS.HasReward = true;
                }
            }
            else
            {
                locationWS.HasReward = false;
            }
            return locationWS;
        }
        private int gainScoreQRCODE(GainScore gainScore, APP app)
        {
            var parameters = new DatabaseParameter[8];
            parameters[0] = dao.CreateDatabaseParameter("v_netstatus", DatabaseType.Int32, gainScore.NetStatus);
            parameters[1] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, gainScore.UserId);
            parameters[2] = dao.CreateDatabaseParameter("v_hash", DatabaseType.Int64, gainScore.Hash);
            parameters[3] = dao.CreateDatabaseParameter("v_code", DatabaseType.String, gainScore.Code);
            parameters[4] = dao.CreateDatabaseParameter("v_example_at", DatabaseType.DateTime, gainScore.Example_at);
            parameters[5] = dao.CreateDatabaseParameter("v_example_sync_at", DatabaseType.DateTime, gainScore.Example_sync_at);
            parameters[6] = dao.CreateDatabaseParameter("v_app_id", DatabaseType.Int32, app.Id);
            parameters[7] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction("SP_GainScore_QrCode_Insert", parameters));
        }
        private int gainScoreCOUPON(GainScore gainScore, APP app)
        {
            var parameters = new DatabaseParameter[15];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, gainScore.UserId);
            parameters[1] = dao.CreateDatabaseParameter("v_cnpj", DatabaseType.String, gainScore.Coupon.Document.CNPJ);
            if (gainScore.Coupon.Document.CPFIsValid(gainScore.Coupon.Document.CPF))
            {
                parameters[2] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, gainScore.Coupon.Document.CPF);
            }
            else
            {
                parameters[2] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, DBNull.Value);
            }
            parameters[3] = dao.CreateDatabaseParameter("v_fiscal_id", DatabaseType.Int32, gainScore.Coupon.Company.Id);
            parameters[4] = dao.CreateDatabaseParameter("v_cod", DatabaseType.Int32, gainScore.Coupon.Cod);
            parameters[5] = dao.CreateDatabaseParameter("v_api", DatabaseType.Int32, gainScore.Coupon.API);
            parameters[6] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, gainScore.Coupon.At);
            parameters[7] = dao.CreateDatabaseParameter("v_sync_at", DatabaseType.DateTime, gainScore.Coupon.SyncAt);
            parameters[8] = dao.CreateDatabaseParameter("v_app_id", DatabaseType.Int32, app.Id);
            parameters[9] = dao.CreateDatabaseParameter("v_qrCode", DatabaseType.String, gainScore.Coupon.QrCode);
            parameters[10] = dao.CreateDatabaseParameter("v_value", DatabaseType.Double, gainScore.Coupon.Value);
            parameters[11] = dao.CreateDatabaseParameter("v_netstatus", DatabaseType.Int32, gainScore.NetStatus);
            parameters[12] = dao.CreateDatabaseParameter("v_employee", DatabaseType.Int32, gainScore.Coupon.Employee);
            parameters[13] = dao.CreateDatabaseParameter("v_efc", DatabaseType.String, gainScore.Coupon.ECF);
            parameters[14] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction("SP_GainScore_Coupon_Insert", parameters));
        }
        private int gainScoreBAR(GainScore gainScore, APP app)
        {
            var parameters = new DatabaseParameter[14];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, gainScore.UserId);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, gainScore.LocationId);
            parameters[2] = dao.CreateDatabaseParameter("v_fiscal_id", DatabaseType.Int32, gainScore.Coupon.Company.Id);
            parameters[3] = dao.CreateDatabaseParameter("v_cod", DatabaseType.Int32, gainScore.Coupon.Cod);
            parameters[4] = dao.CreateDatabaseParameter("v_api", DatabaseType.Int32, gainScore.Coupon.API);
            parameters[5] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, gainScore.Coupon.At);
            parameters[6] = dao.CreateDatabaseParameter("v_sync_at", DatabaseType.DateTime, gainScore.Coupon.SyncAt);
            parameters[7] = dao.CreateDatabaseParameter("v_app_id", DatabaseType.Int32, app.Id);
            parameters[8] = dao.CreateDatabaseParameter("v_qrCode", DatabaseType.String, gainScore.Coupon.QrCode);
            parameters[9] = dao.CreateDatabaseParameter("v_value", DatabaseType.Double, gainScore.Coupon.Value);
            parameters[10] = dao.CreateDatabaseParameter("v_netstatus", DatabaseType.Int32, gainScore.NetStatus);
            parameters[11] = dao.CreateDatabaseParameter("v_employee", DatabaseType.Int32, gainScore.Coupon.Employee);
            parameters[12] = dao.CreateDatabaseParameter("v_efc", DatabaseType.String, gainScore.Coupon.ECF);
            parameters[13] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction("SP_GainScore_Bar_Insert", parameters));
        }
        private int gainScoreNFCe(GainScore gainScore, APP app)
        {
            var parameters = new DatabaseParameter[14];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, gainScore.UserId);
            parameters[1] = dao.CreateDatabaseParameter("v_cnpj", DatabaseType.String, gainScore.Coupon.Document.CNPJ);
            if (gainScore.Coupon.Document.CPFIsValid(gainScore.Coupon.Document.CPF))
            {
                parameters[2] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, gainScore.Coupon.Document.CPF);
            }
            else
            {
                parameters[2] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, DBNull.Value);
            }
            parameters[3] = dao.CreateDatabaseParameter("v_cod", DatabaseType.Int32, gainScore.Coupon.Cod);
            parameters[4] = dao.CreateDatabaseParameter("v_api", DatabaseType.Int32, gainScore.Coupon.API);
            parameters[5] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, gainScore.Coupon.At);
            parameters[6] = dao.CreateDatabaseParameter("v_sync_at", DatabaseType.DateTime, gainScore.Coupon.SyncAt);
            parameters[7] = dao.CreateDatabaseParameter("v_app_id", DatabaseType.Int32, app.Id);
            parameters[8] = dao.CreateDatabaseParameter("v_qrCode", DatabaseType.String, gainScore.Coupon.QrCode);
            parameters[9] = dao.CreateDatabaseParameter("v_value", DatabaseType.Double, gainScore.Coupon.Value);
            parameters[10] = dao.CreateDatabaseParameter("v_netstatus", DatabaseType.Int32, gainScore.NetStatus);
            parameters[11] = dao.CreateDatabaseParameter("v_employee", DatabaseType.Int32, gainScore.Coupon.Employee);
            parameters[12] = dao.CreateDatabaseParameter("v_efc", DatabaseType.String, gainScore.Coupon.ECF);
            parameters[13] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction("SP_GainScore_NFCe_Insert", parameters));
        }
        public int Gain(GainScore gainScore, APP app)
        {
            if (gainScore.FROM == GainScore.GainScoreType.PALETA)
            {
                return gainScoreQRCODE(gainScore, app);
            }
            else if (gainScore.FROM == GainScore.GainScoreType.COUPON)
            {
                return gainScoreCOUPON(gainScore, app);
            }
            else if (gainScore.FROM == GainScore.GainScoreType.BAR)
            {
                return gainScoreBAR(gainScore, app);
            }
            else if (gainScore.FROM == GainScore.GainScoreType.NFCe)
            {
                return gainScoreNFCe(gainScore, app);
            }
            else
            {
                throw new Exception("Tipo de pontuação inválido: " + (int)gainScore.FROM);
            }
        }
    }
}
