﻿using API.Models.Entidades.Account;
using BD.IRepositorios;
using Global.Entidades;
using Global.Utils;
using System;
using System.Data.Common;

namespace API.Models.Repositorios
{
    public class RepUser : Repositorio
    {
        public void Customer(int userId, int LocationId)
        {
            string command = "SP_Location_Custumer_Insert";
            DatabaseParameter[] parameters = new DatabaseParameter[3];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, userId);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, LocationId);
            parameters[2] = dao.CreateDatabaseParameter("v_at", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            dao.RunProcedure(command, parameters);
        }
        public User Photo(User user)
        {
            string command = "SP_User_Photo";
            DatabaseParameter[] parameters = new DatabaseParameter[2];
            parameters[0] = dao.CreateDatabaseParameter("v_photo_url", DatabaseType.String, user.PhotoURL);
            parameters[1] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user.Id);
            dao.RunProcedure(command, parameters);
            return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
        }
        public User Save(User user)
        {
            string command = "SP_User_Insert";
            DatabaseParameter[] parameters = new DatabaseParameter[14];
            if (string.IsNullOrWhiteSpace(user.Email))
            {
                parameters[0] = dao.CreateDatabaseParameter("v_email", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[0] = dao.CreateDatabaseParameter("v_email", DatabaseType.String, user.Email);
            }
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                parameters[1] = dao.CreateDatabaseParameter("v_name", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[1] = dao.CreateDatabaseParameter("v_name", DatabaseType.String, user.Name);
            }
            if (string.IsNullOrWhiteSpace(user.Password))
            {
                parameters[2] = dao.CreateDatabaseParameter("v_password", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[2] = dao.CreateDatabaseParameter("v_password", DatabaseType.String, user.Password);
            }
            parameters[3] = dao.CreateDatabaseParameter("v_signUpAt", DatabaseType.DateTime, Auxiliar.GetCurrentDateTime());
            if (string.IsNullOrWhiteSpace(user.Gender))
            {
                parameters[4] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[4] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, user.Gender);
            }
            if (user.Birthday == null || user.Birthday == DateTime.MinValue)
            {
                parameters[5] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, DBNull.Value);
            }
            else
            {
                parameters[5] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, user.Birthday);
            }
            if (user.Document == null || string.IsNullOrWhiteSpace(user.Document.CPF))
            {
                parameters[6] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[6] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, user.Document.CPF);
            }
            parameters[7] = dao.CreateDatabaseParameter("v_facebookId", DatabaseType.Int64, user.FacebookId);
            if (string.IsNullOrWhiteSpace(user.Hometown))
            {
                parameters[8] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[8] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, user.Hometown);
            }
            if (string.IsNullOrWhiteSpace(user.Location))
            {
                parameters[9] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[9] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, user.Location);
            }
            if (string.IsNullOrWhiteSpace(user.Locale))
            {
                parameters[10] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[10] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, user.Locale);
            }
            if (string.IsNullOrWhiteSpace(user.FacebookAccessToken))
            {
                parameters[11] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, string.Empty);
            }
            else
            {
                parameters[11] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, user.FacebookAccessToken);
            }
            parameters[12] = dao.CreateDatabaseParameter("v_facebookAccessExpires", DatabaseType.Int64, user.FacebookAccessExpires);
            parameters[13] = dao.CreateDatabaseParameterOutput("v_id", DatabaseType.Int32);
            int user_id = (int)dao.RunFunction(command, parameters);
            return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user_id));
        }
        public User Profile(User user)
        {
            string command = "SP_User_Insert_Profile";
            DatabaseParameter[] parameters = new DatabaseParameter[11];
            parameters[0] = dao.CreateDatabaseParameter("v_accountId", DatabaseType.Int32, user.IdAccount);
            if (user.Birthday == null || user.Birthday == DateTime.MinValue)
            {
                parameters[1] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, DBNull.Value);
            }
            else
            {
                parameters[1] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, user.Birthday);
            }
            if (string.IsNullOrWhiteSpace(user.Gender))
            {
                parameters[2] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[2] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, user.Gender);
            }
            if (user.Document == null || string.IsNullOrWhiteSpace(user.Document.CPF))
            {
                parameters[3] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[3] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, user.Document.CPF);
            }
            parameters[4] = dao.CreateDatabaseParameter("v_facebookId", DatabaseType.Int64, user.FacebookId);
            if (string.IsNullOrWhiteSpace(user.Hometown))
            {
                parameters[5] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[5] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, user.Hometown);
            }
            if (string.IsNullOrWhiteSpace(user.Location))
            {
                parameters[6] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[6] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, user.Location);
            }
            if (string.IsNullOrWhiteSpace(user.Locale))
            {
                parameters[7] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[7] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, user.Locale);
            }
            if (string.IsNullOrWhiteSpace(user.FacebookAccessToken))
            {
                parameters[8] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, string.Empty);
            }
            else
            {
                parameters[8] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, user.FacebookAccessToken);
            }
            parameters[9] = dao.CreateDatabaseParameter("v_facebookAccessExpires", DatabaseType.Int64, user.FacebookAccessExpires);
            parameters[10] = dao.CreateDatabaseParameterOutput("v_id", DatabaseType.Int32);
            int user_id = DAOReturn.StatusOfTransaction((int)dao.RunFunction(command, parameters));
            return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user_id));
        }
        public User Find(SearchAccount search)
        {
            var parameters = new DatabaseParameter[1];
            if (search.Type == SearchAccount.ParameterType.Id)
            {
                parameters[0] = dao.CreateDatabaseParameter("v_id", DatabaseType.Int32, search.USER_ID);
                var dr = dao.RunSelectProcedure("FN_User_Id", parameters);
                return BuildObject.Build<User>(dr);
            }
            else if (search.Type == SearchAccount.ParameterType.EMAIL)
            {
                parameters[0] = dao.CreateDatabaseParameter("v_email", DatabaseType.String, search.EMAIL);
                var dr = dao.RunSelectProcedure("FN_User_Email", parameters);
                return BuildObject.Build<User>(dr);
            }
            else if (search.Type == SearchAccount.ParameterType.CPF)
            {
                parameters[0] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, search.CPF);
                var dr = dao.RunSelectProcedure("FN_User_CPF", parameters);
                return BuildObject.Build<User>(dr);
            }
            else if (search.Type == SearchAccount.ParameterType.FACEBOOK_ID)
            {
                parameters[0] = dao.CreateDatabaseParameter("v_facebookId", DatabaseType.Int64, search.FACEBOOK_ID);
                var dr = dao.RunSelectProcedure("FN_User_FacebookId", parameters);
                return BuildObject.Build<User>(dr);
            }
            else if (search.Type == SearchAccount.ParameterType.FACEBOOK_SCOOPED_ID)
            {
                parameters[0] = dao.CreateDatabaseParameter("v_scoped", DatabaseType.Int64, search.FACEBOOK_SCOOPED_ID);
                var dr = dao.RunSelectProcedure("FN_User_ScopedId", parameters);
                return BuildObject.Build<User>(dr);
            }
            else
            {
                throw new Exception("Tipo inválido de busca");
            }
         }
         public User Update(User user)
         {
            string command = "SP_User_Update";
            var parameters = new DatabaseParameter[12];
            parameters[0] = dao.CreateDatabaseParameter("v_iduser", DatabaseType.Int32, user.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_facebookId", DatabaseType.Int64, user.FacebookId);
            parameters[2] = dao.CreateDatabaseParameter("v_name", DatabaseType.String, user.Name);

            if (user.Birthday == DateTime.MinValue)
            {
                parameters[3] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, DBNull.Value);
            }
            else
            {
                parameters[3] = dao.CreateDatabaseParameter("v_birthday", DatabaseType.DateTime, user.Birthday);
            }
            if (user.Hometown == null)
            {
                parameters[4] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[4] = dao.CreateDatabaseParameter("v_hometown", DatabaseType.String, user.Hometown);
            }
            if (user.Location == null)
            {
                parameters[5] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[5] = dao.CreateDatabaseParameter("v_location", DatabaseType.String, user.Location);
            }
            if (user.Gender == null)
            {
                parameters[6] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[6] = dao.CreateDatabaseParameter("v_gender", DatabaseType.String, user.Gender);
            }
            if (user.Locale == null)
            {
                parameters[7] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[7] = dao.CreateDatabaseParameter("v_locale", DatabaseType.String, user.Locale);
            }
            if (string.IsNullOrWhiteSpace(user.FacebookAccessToken))
            {
                parameters[8] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, string.Empty);
            }
            else
            {
                parameters[8] = dao.CreateDatabaseParameter("v_facebookAccessToken", DatabaseType.String, user.FacebookAccessToken);
            }
            parameters[9] = dao.CreateDatabaseParameter("v_facebookAccessExpires", DatabaseType.Int64, user.FacebookAccessExpires);
            if (string.IsNullOrWhiteSpace(user.CPF))
            {
                parameters[10] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[10] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, user.CPF);
            }
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                parameters[11] = dao.CreateDatabaseParameter("v_email", DatabaseType.String, user.Email);
            }
            else
            {
                parameters[11] = dao.CreateDatabaseParameter("v_email", DatabaseType.String, DBNull.Value);
            }
            dao.RunProcedure(command, parameters);

            return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
        }
        public User UpdateCPF(User user)
        {
            var parameters = new DatabaseParameter[2];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, user.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_cpf", DatabaseType.String, user.CPF);
            dao.RunProcedure("SP_User_Update_cpf", parameters);
            return this.Find(new SearchAccount(SearchAccount.ObjectType.User, SearchAccount.ParameterType.Id, user.Id));
        }
    }
}
