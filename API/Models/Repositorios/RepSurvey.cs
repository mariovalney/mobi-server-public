﻿using API.Models.Entidades.Rating;
using BD.IRepositorios;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace API.Models.Repositorios
{
    public class RepSurvey : Repositorio
    {
        public int Appreciation(Appreciation appreciation, DateTime date)
        {
            String command = "SP_Appreciation_Insert";
            DatabaseParameter[] parameters = new DatabaseParameter[9];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, appreciation.User.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_location_id", DatabaseType.Int32, appreciation.EstablishmentLocation.Id);
            parameters[2] = dao.CreateDatabaseParameter("v_store_item_id", DatabaseType.Int32, DBNull.Value);
            parameters[3] = dao.CreateDatabaseParameter("v_NSI", DatabaseType.Int32, appreciation.NSI);
            parameters[4] = dao.CreateDatabaseParameter("v_NPS", DatabaseType.Int32, appreciation.NPS);
            parameters[5] = dao.CreateDatabaseParameter("v_display_app", DatabaseType.Bit, 1);
            if (appreciation.Comment == null || appreciation.Comment == "")
            {
                parameters[6] = dao.CreateDatabaseParameter("v_comment", DatabaseType.String, DBNull.Value);
            }
            else
            {
                parameters[6] = dao.CreateDatabaseParameter("v_comment", DatabaseType.String, appreciation.Comment);
            }
            parameters[7] = dao.CreateDatabaseParameter("v_appreciation_at", DatabaseType.DateTime, date);
            parameters[8] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction(command, parameters));
        }
        public int Answer(SurveyAnswer answer, DateTime date)
        {
            String command = "SP_SurveyAnswer_Insert";
            DatabaseParameter[] parameters = new DatabaseParameter[7];
            parameters[0] = dao.CreateDatabaseParameter("v_user_id", DatabaseType.Int32, answer.User.Id);
            parameters[1] = dao.CreateDatabaseParameter("v_answer_text", DatabaseType.Int32, DBNull.Value);
            parameters[2] = dao.CreateDatabaseParameter("v_answer_at", DatabaseType.DateTime, date);
            parameters[3] = dao.CreateDatabaseParameter("v_answer_choice", DatabaseType.Int32, DBNull.Value);
            if (answer.ScoreAnswer < 0 || answer.ScoreAnswer > 5)
            {
                parameters[4] = dao.CreateDatabaseParameter("v_asnwer_score", DatabaseType.Int32, DBNull.Value);
            }
            else
            {
                parameters[4] = dao.CreateDatabaseParameter("v_asnwer_score", DatabaseType.Int32, answer.ScoreAnswer);
            }
            parameters[5] = dao.CreateDatabaseParameter("v_survey_selected_id", DatabaseType.Int32, answer.SurveySelected.Id);
            parameters[6] = dao.CreateDatabaseParameterOutput("v_out", DatabaseType.Int32);
            return DAOReturn.StatusOfTransaction((int)dao.RunFunction(command, parameters));
        }
        public Rating Rating(int locationId)
        {
            var rating = default(Rating);
            var ratingTemp = new Rating();

            var parameters = new DatabaseParameter[1];
            parameters[0] = dao.CreateDatabaseParameter("location_id", DatabaseType.Int32, locationId);
            var dr = dao.RunSelectProcedure("FN_WS_Survey_Select_Location", parameters);

            while (dr.Read())
            {
                ratingTemp = LoadRating(dr);
                if (rating == default(Rating))
                {
                    rating = new Rating();
                    rating.SurveyQuestions = new List<SurveyQuestion>();
                    rating.Id = ratingTemp.Id;
                    rating.HasAppreciation = ratingTemp.HasAppreciation;
                }
                foreach (var item in ratingTemp.SurveyQuestions)
                {
                    if (rating.SurveyQuestions.Find(x => x.IdToAnswer == item.IdToAnswer) == null)
                    {
                        var question = new SurveyQuestion();
                        question.Id = item.IdToAnswer;
                        question.IdToAnswer = item.IdToAnswer;
                        question.Question = item.Question;
                        question.Type = item.Type;
                        rating.SurveyQuestions.Add(question);
                    }
                }
            }
            return rating;
        }

        private Rating LoadRating(DbDataReader dr)
        {
            var rating = new Rating();
            var question = new SurveyQuestion();
            var list_of_questions = new List<SurveyQuestion>();
            
            if (!dr.IsDBNull("SurveyId"))
            {
                rating.Id = dr.GetInt32("SurveyId");
            }
            if (!dr.IsDBNull("HasAppreciation"))
            {
                rating.HasAppreciation = dr.GetBoolean("HasAppreciation");
            }
            if (!dr.IsDBNull("QuestionId"))
            {
                question.Id = dr.GetInt32("QuestionId");
            }
            if (!dr.IsDBNull("IdToAnswer"))
            {
                question.IdToAnswer = dr.GetInt32("IdToAnswer");
            }
            if (!dr.IsDBNull("SurveyQuestionText"))
            {
                question.Question = dr.GetString("SurveyQuestionText");
            }
            if (!dr.IsDBNull("SurveyQuestionType"))
            {
                question.Type = dr.GetInt32("SurveyQuestionType");
            }
            list_of_questions.Add(question);
            rating.SurveyQuestions = list_of_questions;
            return rating;
        }
    }
}
