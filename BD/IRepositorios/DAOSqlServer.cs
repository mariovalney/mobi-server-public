﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Threading;

namespace BD.IRepositorios
{
    public class DAOSqlServer : DAO
    {

        private static readonly String CLASS_NAME = "DAOSqlServer";

        #region Atributos
        private SqlConnectionStringBuilder csb;
        private int level;
        #endregion

        #region Propriedades
        public override DbConnection Conn { get; set; }
        #endregion

        #region Constantes

        private static readonly string SERVER_NAME = "";
        private static readonly string DATABASE_NAME = "";
        private static readonly string USER_NAME = "";
        private static readonly string PASSWORD = "";
        private static readonly string TRUSTED_CONNECTION = "";
        #endregion

        #region Construtor
        /// <summary>
        /// contrutor para carregamento da Connection String
        /// </summary>
        public DAOSqlServer()
        {
            this.csb = new SqlConnectionStringBuilder();
            this.csb.ConnectionString =
                "Server=" + SERVER_NAME + ";" +
                "Database=" + DATABASE_NAME + ";" +
                "User ID=" + USER_NAME + ";" +
                "Password=" + PASSWORD + ";" +
                "Trusted_Connection=" + TRUSTED_CONNECTION + ";" +
                "MultipleActiveResultSets=True;";
        }
        #endregion

        #region Metodos

        #region Metodos Comandos

        /// <summary>
        /// Executa uma Stored Procedure
        /// </summary>
        /// <param name="parameters">paramentros da procedure</param>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns></returns>
        public override bool RunProcedure(string procedureName, DatabaseParameter[] parameters)
        {
            try
            {
                this.OpenConnection();
                SqlCommand comm = this.BuilCommand(procedureName, parameters);
                comm.CommandType = CommandType.StoredProcedure;

                comm.ExecuteNonQuery();

                return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Executa procedure com o ultimo parametro sendo do tipo out e ele é retornado
        /// </summary>
        /// <param name="parameters">parametros da procedure</param>
        /// <param name="functionName">nome da funcao</param>
        /// <returns>tipo retornado</returns>
        public override object RunFunction(string functionName, DatabaseParameter[] parameters)
        {
            try
            {
                this.OpenConnection();
                SqlCommand comm = this.BuilCommand(functionName, parameters);
                comm.CommandType = CommandType.StoredProcedure;
                comm.ExecuteNonQuery();

                SqlParameter returnValue = comm.Parameters[comm.Parameters.Count - 1];

                return returnValue.Value;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Procedure com Select
        /// </summary>
        /// <param name="parameters">parametros da procedure</param>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns>Data Reader com o resultado da </returns>
        public override DbDataReader RunSelectProcedure(string procedureName, DatabaseParameter[] parameters)
        {
            try
            {

                this.OpenConnection();

                String functionComand = procedureName.Substring(2);
                functionComand = "select * from FN" + functionComand + "(";
                for (int i = 0; i < parameters.Length; i++)
                {
                    functionComand = functionComand + "@" + parameters[i].ParameterName + ",";
                }

                functionComand = functionComand.Substring(0, functionComand.Length - 1) + ");";

                SqlCommand comm = this.BuilCommand(functionComand, parameters);

                SqlDataReader dr = comm.ExecuteReader();

                return dr;
            }
            catch (Exception exc)
            {
                this.CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Procedure com Select sem parametros
        /// </summary>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns>Data reader com o resultado do select</returns>
        public override DbDataReader RunSelectProcedure(string procedureName)
        {
            try
            {
                this.OpenConnection();

                String functionComand = procedureName.Substring(2);
                functionComand = "select * from FN" + functionComand + "();";

                SqlCommand comm = this.BuildCommand(functionComand);
                SqlDataReader dr = comm.ExecuteReader();

                return dr;
            }
            catch (Exception)
            {
                this.CloseConnection();
                throw;
            }
        }


        #endregion

        #region Metodo Conexao
        public override void OpenConnection()
        {
            if (Conn == null)
            {
                Conn = new SqlConnection(csb.ConnectionString);
                Conn.Open();
            }

            while (Conn.State != ConnectionState.Open)
            {
                if (Conn.State == ConnectionState.Broken)
                {
                    Conn.Close();
                    Conn.Open();
                }
                else if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }

                Thread.Sleep(1000);
            }

            level = level + 1;
        }
        public override void CloseConnection()
        {
            level = level - 1;

            if (level <= 0)
            {
                if (Conn != null)
                {
                    if (Conn.State != ConnectionState.Closed)
                    {
                        Conn.Close();
                        Conn.Dispose();
                        Conn = null;
                    }
                }
                level = 0;
            }
        }
        public override void CloseConnectionDataReader(DbDataReader dr)
        {
            if (dr != null)
            {
                dr.Close();
            }

            this.CloseConnection();
        }
        #endregion

        #endregion

        #region Metodos Internos
        private SqlCommand BuilCommand(string commandName, DatabaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandName, (SqlConnection) this.Conn);

            if (parameters != null)
            {
                command.Parameters.AddRange(this.DatabaseParametersToSqlParameters(parameters));
            }

            return command;
        }
        private SqlCommand BuildCommand(string commandName)
        {
            return this.BuilCommand(commandName, null);
        }

        private SqlParameter DatabaseParameterToSqlParameter(DatabaseParameter parameter)
        {
            SqlParameter retorno = null;

            if (parameter.Direction == ParameterDirection.Input)
            {
                retorno = new SqlParameter("@" + parameter.ParameterName, this.DatabaseTypeToSqlDbType(parameter.Type));
                retorno.Direction = ParameterDirection.Input;
                retorno.Value = parameter.Value;
            }
            else if (parameter.Direction == ParameterDirection.Output)
            {
                retorno = new SqlParameter("@" + parameter.ParameterName, this.DatabaseTypeToSqlDbType(parameter.Type));
                retorno.Direction = ParameterDirection.Output;
                if (parameter.Size > 0)
                {
                    retorno.Size = parameter.Size;
                }
            }

            return retorno;
        }
        private SqlParameter[] DatabaseParametersToSqlParameters(DatabaseParameter[] parameters)
        {
            SqlParameter[] retorno = new SqlParameter[parameters.Length];

            for (int i = 0; i < parameters.Length; i++)
            {
                retorno[i] = this.DatabaseParameterToSqlParameter(parameters[i]);
            }

            return retorno;
        }

        private SqlDbType DatabaseTypeToSqlDbType(DatabaseType type)
        {
            switch (type)
            {
                case DatabaseType.Bit:
                    return SqlDbType.Bit;
                case DatabaseType.Byte:
                    return SqlDbType.TinyInt;
                case DatabaseType.DateTime:
                    return SqlDbType.DateTime;
                case DatabaseType.Double:
                    return SqlDbType.Float;
                case DatabaseType.Int32:
                    return SqlDbType.Int;
                case DatabaseType.Int64:
                    return SqlDbType.BigInt;
                case DatabaseType.String:
                    return SqlDbType.VarChar;
                case DatabaseType.Time:
                    return SqlDbType.Time;
                default:
                    return SqlDbType.Int;
            }
        }
        #endregion
    }
}
