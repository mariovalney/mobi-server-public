﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace BD.IRepositorios
{
    public static class ExtensoesDAO
    {
        public static bool IsDBNull(this DbDataReader dr, string field)
        {
            return dr[field] == DBNull.Value;
        }
        public static String GetString(this DbDataReader dr, string field)
        {
            return dr.GetString(dr.GetOrdinal(field));
        }
        public static double GetMoney(this DbDataReader dr, string field)
        {
            return (double) dr.GetDecimal(dr.GetOrdinal(field));
        }

        public static double GetDouble(this DbDataReader dr, string field)
        {
            return dr.GetDouble(dr.GetOrdinal(field));
        }

        public static int GetInt32(this DbDataReader dr, string field)
        {
            return dr.GetInt32(dr.GetOrdinal(field));
        }

        public static long GetInt64(this DbDataReader dr, string field)
        {
            return dr.GetInt64(dr.GetOrdinal(field));
        }

        public static DateTime GetDateTime(this DbDataReader dr, string field)
        {
            return dr.GetDateTime(dr.GetOrdinal(field));
        }

        public static bool GetBoolean(this DbDataReader dr, string field)
        {
            return dr.GetBoolean(dr.GetOrdinal(field));
        }
        public static TimeSpan GetTime(this DbDataReader dr, string field)
        {
            return (TimeSpan)dr[field];
        }
    }
}
