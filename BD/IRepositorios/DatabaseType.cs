﻿using System;

namespace BD.IRepositorios
{
    public enum DatabaseType
    {
        Int32 = 0,
        Int64 = 1,
        Byte = 2,
        Bit = 3,
        Double = 4,
        DateTime = 5,
        String = 6,
        Time = 7,
    }
}
