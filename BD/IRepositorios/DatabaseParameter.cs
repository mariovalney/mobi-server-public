﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BD.IRepositorios
{
    public class DatabaseParameter
    {
        #region Atributos
        private int _size;
        #endregion

        #region Propriedades
        public object Value { get; set; }
        public DatabaseType Type { get; set; }
        public string ParameterName { get; set; }
        public ParameterDirection Direction { get; set; }
        public int Size
        {
            get { return this._size; }
            set { this._size = value; }
        }
        #endregion

        public DatabaseParameter()
        {
            this._size = -1;
        }
    }
}
