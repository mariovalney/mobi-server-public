﻿using System.Data;
using System.Data.Common;

namespace BD.IRepositorios
{
    public abstract class DAO
    {
        private static DAO _instance;
        
        #region Propriedades
        public abstract DbConnection Conn { get; set; }
        public static DAO Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DAOSqlServer();
                }

                return _instance;
            }
        } 
        #endregion

        #region Metodos

        #region Metodos Comandos

        /// <summary>
        /// Executa uma Stored Procedure
        /// </summary>
        /// <param name="parameters">paramentros da procedure</param>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns></returns>
        public abstract bool RunProcedure(string procedureName, DatabaseParameter[] parameters);

        /// <summary>
        /// Executa procedure com o ultimo parametro sendo do tipo out e ele é retornado
        /// </summary>
        /// <param name="parameters">parametros da procedure</param>
        /// <param name="functionName">nome da funcao</param>
        /// <returns>tipo retornado</returns>
        public abstract object RunFunction(string functionName, DatabaseParameter[] parameters);

        /// <summary>
        /// Procedure com Select
        /// </summary>
        /// <param name="parameters">parametros da procedure</param>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns>Data Reader com o resultado da </returns>
        public abstract DbDataReader RunSelectProcedure(string procedureName, DatabaseParameter[] parameters);

        /// <summary>
        /// Procedure com Select sem parametros
        /// </summary>
        /// <param name="procedureName">nome da procedure</param>
        /// <returns>Data reader com o resultado do select</returns>
        public abstract DbDataReader RunSelectProcedure(string procedureName);

        #endregion

        #region Metodo Parametros
        public DatabaseParameter CreateDatabaseParameter(string parameterName, DatabaseType dbType, object value)
        {
            DatabaseParameter retorno = new DatabaseParameter();
            retorno.ParameterName = parameterName;
            retorno.Type = dbType;
            retorno.Direction = ParameterDirection.Input;
            retorno.Value = value;

            return retorno;
        }
        public DatabaseParameter CreateDatabaseParameterOutput(string parameterName, DatabaseType dbType)
        {
            DatabaseParameter retorno = new DatabaseParameter();
            retorno.ParameterName = parameterName;
            retorno.Type = dbType;
            retorno.Direction = ParameterDirection.Output;

            return retorno;
        }
        public DatabaseParameter CreateDatabaseParameterOutput(string parameterName, DatabaseType dbType, int size)
        {
            DatabaseParameter retorno = new DatabaseParameter();
            retorno.ParameterName = parameterName;
            retorno.Type = dbType;
            retorno.Direction = ParameterDirection.Output;
            retorno.Size = size;

            return retorno;
        }
        #endregion        

        #region Metodo Conexao
        public abstract void OpenConnection();
        public abstract void CloseConnection();
        public abstract void CloseConnectionDataReader(DbDataReader dr);
        #endregion

        #endregion
    }
}
