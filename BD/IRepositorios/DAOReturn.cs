﻿using Error;
using System;

namespace BD.IRepositorios
{
    public class DAOReturn
    {
        public static int StatusOfTransaction(int returnedValue)
        {
            if (returnedValue >= 0)
            {
                return returnedValue;
            }
            //-1
            else if (returnedValue == (int)DatabaseError.AccountNotFound)
            {
                throw new AccountNotFoundException("O usuário não foi encontrado.");
            }
            //-2
            else if (returnedValue == (int)DatabaseError.AccountBlocked)
            {
                throw new AccountBlockedException("O usuário encontra-se bloqueado.");
            }
            //-3
            else if (returnedValue == (int)DatabaseError.QrCodeNotFound)
            {
                throw new QrCodeNotFoundException("O código informado não foi encontrado.");
            }
            //-4
            else if (returnedValue == (int)DatabaseError.QrcodeWrongDay)
            {
                throw new QrCodeInvalidDayException("O código informado não é válido hoje.");
            }
            //-5
            else if (returnedValue == (int)DatabaseError.QrCodeDisabled)
            {
                throw new QrCodeNotEnabledException("O código informado encontra-se desativado.");
            }
            //-6
            else if (returnedValue == (int)DatabaseError.QrCodeDiscarded)
            {
                throw new QrCodeNotEnabledException("O código informado encontra-se recolhido.");
            }
            //-7
            else if (returnedValue == (int)DatabaseError.RuleNotFound)
            {
                throw new RuleNotFoundException("A regra de pontuação não foi cadastrada.");
            }
            //-8
            else if (returnedValue == (int)DatabaseError.LimitOfExampleToday)
            {
                throw new DailyLimitReachedException("O usuário atingiu a cota maxima de pontos.");
            }
            //-9
            else if (returnedValue == (int)DatabaseError.MobyAlreadyRegister)
            {
                throw new ExampleAlreadyRegistredException("O ponto já encontra-se sincronizado.");
            }
            //-10
            else if (returnedValue == (int)DatabaseError.ShoppingNotFound)
            {
                throw new ShoppingNotFoundException("O clube não foi cadastrado.");
            }
            //-11
            else if (returnedValue == (int)DatabaseError.LocationNotFound)
            {
                throw new LocationNotFoundException("Loja não encontrada.");
            }
            //-12
            else if (returnedValue == (int)DatabaseError.ManagerNotFound)
            {
                throw new ManagerNotFoundException("Administrador não encontrado.");
            }
            //-13
            else if (returnedValue == (int)DatabaseError.ManagerBlocked)
            {
                throw new ManagerBlockedException("Administrador bloqueado.");
            }
            //-14
            else if (returnedValue == (int)DatabaseError.OwnerNotFount)
            {
                throw new EstablishmentOwnerNotFoundException("Lojista não encontrado.");
            }
            //-15
            else if (returnedValue == (int)DatabaseError.OwnerBlocked)
            {
                throw new ManagerBlockedException("Lojista bloqueado.");
            }
            //-16
            else if (returnedValue == (int)DatabaseError.LocationNotBelongToApp)
            {
                throw new AppLinkEstablishmentNotFound("A loja não pertence ao aplicativo.");
            }
            //-17
            else if (returnedValue == (int)DatabaseError.TypeOfGainScoreNotFound)
            {
                throw new RuleTypeNotDefineException("Tipo de pontuação não foi definido.");
            }
            //-18
            else if (returnedValue == (int)DatabaseError.RuleGapNotFound)
            {
                throw new RuleIntervaloNotDefineException("Intervalo de pontuação não cadastrado.");
            }
            //-19
            else if (returnedValue == (int)DatabaseError.ExamplePlus0)
            {
                throw new ExamplePlusZeroScoreException("Regra possibilita adição de 0 pontos.");
            }
            //-20
            else if (returnedValue == (int)DatabaseError.RequestCPF)
            {
                throw new RequestCPFExeception("CPF não cadastrado.");
            }
            //-21
            else if (returnedValue == (int)DatabaseError.InvalidAPIOfCoupon)
            {
                throw new QrCodeInvalidException("API do cupom fiscal não é válido");
            }
            //-22
            else if (returnedValue == (int)DatabaseError.RewardNotFound)
            {
                throw new RewardNotFoundException("A recompensa não foi encontrada");
            }
            //-23
            else if (returnedValue == (int)DatabaseError.RewardDisabled)
            {
                throw new RewardBlockedException("A recompensa encontra-se desativada");
            }
            //-24
            else if (returnedValue == (int)DatabaseError.RewardNotStarted)
            {
                throw new RewardNotStartedException("A recompensa ainda não está disponível.");
            }
            //-25
            else if (returnedValue == (int)DatabaseError.RewardIsEnded)
            {
                throw new RewardEndedException("A recompensa não encontra-se mais disponível.");
            }
            //-26
            else if (returnedValue == (int)DatabaseError.RewardNotLinked)
            {
                throw new RewardReferenceNotFound("A recompensa solicitada não está vinculada a nenhuma loja ou franquia.");
            }
            //-27
            else if(returnedValue == (int)DatabaseError.RewardNotBelongsToLocation)
            {
                throw new RewardInvalidLocationExeption("A recompensa não pertence a loja informada.");
            }
            //-28
            else if(returnedValue == (int)DatabaseError.RewardLimit)
            {
                throw new SoldOutRewardException("Limite total de recompensas atingido.");
            }
            //-29
            else if(returnedValue == (int)DatabaseError.RewardLimitDay)
            {
                throw new SoldOutRewardShotTodayException("Limite de recompensas para hoje atingido.");
            }
            //-30
            else if(returnedValue == (int)DatabaseError.RewardLimitUser)
            {
                throw new SoldOutRewardShotUserException("Limite de recompensas por usuário.");
            }
            //-31
            else if (returnedValue == (int)DatabaseError.RewardLimitUserDay)
            {
                throw new SoldOutRewardShotUserTodayException("Limite de recompensas para o usuário hoje.");
            }
            //-32
            else if(returnedValue == (int)DatabaseError.RewardRandon)
            {
                throw new RewardRandomException("Probabilidade não alcançada.");
            }
            //-33
            else if (returnedValue == (int)DatabaseError.ExampleNotEnogth)
            {
                throw new InsufficientFundsException("O usuário não possui saldo suficiente.");
            }
            //-34
            else if (returnedValue == (int)DatabaseError.LocationNotEnabled)
            {
                throw new LocationBlockedException("A loja encontra-se desativada.");
            }
            //-35
            else if (returnedValue == (int)DatabaseError.ShoppingNotEnabled)
            {
                throw new ShoppingBlockedException("O shopping encontra-se desativado.");
            }
            //-36
            else if (returnedValue == (int)DatabaseError.RewardExpired)
            {
                throw new RewardExpiredException("A recompensa encontra-se expirada.");
            }
            //-37
            else if (returnedValue == (int)DatabaseError.NotifyNotFound)
            {
                throw new NotifyNotFoundException("A notificação não foi encontrada.");
            }
            //-38
            else if (returnedValue == (int)DatabaseError.NotifyLinkPlaceNotFound)
            {
                throw new NotifyLinkLocationNotFound("A notificação não possui vinculo com franquia ou loja.");
            }
            //-39
            else if (returnedValue == (int)DatabaseError.NotifyLinkUserNotFound)
            {
                throw new NotifyLinkUserException("A notificação não pertence ao usuário informado.");
            }
            //-40
            else if (returnedValue == (int)DatabaseError.NotifyAlreadyRead)
            {
                throw new NotifyAlreadyRead("A notificação já se encontra marcada como lida.");
            }
            //-41
            else if (returnedValue == (int)DatabaseError.NotifyAlreadyDeleted)
            {
                throw new NotifyAlreadyDeleted("A notificação já se encontra marcada como deletada.");
            }
            //-42
            else if (returnedValue == (int)DatabaseError.ReasonNotFound)
            {
                throw new ReasonNotFoundException("A razão para bloqueio notiicações não foi encontrada");
            }
            //-43
            else if (returnedValue == (int)DatabaseError.WrongOperationType)
            {
                throw new InvalidOperationException("Não foi possível determinar o tipo correto de operação");
            }
            //-44
            else if (returnedValue == (int)DatabaseError.RewardLinkUserNotFound)
            {
                throw new RewardLinkUserException("A recompensa não pertence ao usuário informado");
            }
            //-45
            else if (returnedValue == (int)DatabaseError.RewardAlreadyAccepted)
            {
                throw new RewardAlreadyAccepted("A recompensa já foi aceita.");
            }
            //-46
            else if (returnedValue == (int)DatabaseError.RewardAlreadyDiscarted)
            {
                throw new RewardAlreadyDiscarted("A recompensa já foi deletada.");
            }
            //-47
            else if (returnedValue == (int)DatabaseError.UserProfileAlreadyExist)
            {
                throw new UserProfileAlreadyExistsException("O perfil do usuário já existe");
            }
            //-48
            else if (returnedValue == (int)DatabaseError.RewardNotWin)
            {
               throw new  RewardNotWinException("O usuário não adquiriu o prêmio inválido (win = 0)");
            }
            //-49
            else if(returnedValue == (int)DatabaseError.RewardAlreadyRescued)
            {
                throw new RewardAlreadyRescuedException("A recompensa já foi resgatada");
            }
            //-50
            else if (returnedValue == (int)DatabaseError.QrCodeGainDisable)
            {
                throw new QrCodeNotEnabledException("O código informado encontra-se desativado para pontuação. Apenas resgate está autorizado.");
            }
            //-51
            else if (returnedValue == (int)DatabaseError.SurveyAlreadyRegister)
            {
                throw new SurveyAlreadyRegister("A avaliação já foi registrada");
            }
            //-52
            else if (returnedValue == (int)DatabaseError.EstablishmentBlocked)
            {
                throw new EstablishmentBlockedException("A avaliação já foi registrada");
            }
            //-53
            else if (returnedValue == (int)DatabaseError.RewardNotBelongToApp)
            {
                throw new RewardLinkAppException("A recompensa não pertence ao aplicativo informado");
            }
            //-54
            else if (returnedValue == (int)DatabaseError.LimitOfExampleByVisit)
            {
                throw new VisitLimitReachedException();
            }
            //-55
            else if (returnedValue == (int)DatabaseError.CouponExp)
            {
                throw new FiscalCouponExpiredException();
            }
            //-56
            else if (returnedValue == (int)DatabaseError.NfceCPF)
            {
                throw new NfceCPFNotFoundException();
            }
            //-57
            else if (returnedValue == (int)DatabaseError.ShouldEqualCPF)
            {
                throw new UserCPFAndNFCeCPFException();
            }
            return returnedValue;
        }
    }
}
