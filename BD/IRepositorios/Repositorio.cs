﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BD.IRepositorios
{
    public abstract class Repositorio
    {
        #region Atributo
        protected DAO dao;
        //protected DAOSqlServer dao;
        #endregion

        #region Construtor
        public Repositorio()
        {
            dao = DAO.Instance;
            //dao = DAOSqlServer.Instance;
        } 
        #endregion
    }
}
